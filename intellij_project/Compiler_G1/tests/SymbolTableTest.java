import Lexicon.ConstValues;
import Lexicon.Token;
import Symbols.SymbolTable;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class SymbolTableTest {

    private ArrayList<Token> tokens;
    private SymbolTable symbolTable;

    public void initTokensGlobal(){
        tokens = new ArrayList<Token>();
        for (int i = 0; i < 10; i++){
            Token token = new Token(ConstValues.INTG_TYPEID_VALUE, "var" + i);
            token.setScope(ConstValues.GLOBAL_SCOPE);
            tokens.add(token);
        }
    }

    public void initTokensGlobalAndMain(){
        tokens = new ArrayList<Token>();
        for (int i = 0; i < 10; i++){
            Token token = new Token(ConstValues.INTG_TYPEID_VALUE, "var" + i);
            if (i % 2 == 0){
                token.setScope(ConstValues.MAIN_SCOPE);
            }else{
                token.setScope(ConstValues.GLOBAL_SCOPE);
            }
            tokens.add(token);
        }
    }

    public void initTokensMix(){
        tokens = new ArrayList<Token>();
        int j = 0;
        for (int i = 0; i < 10; i++){
            Token token = new Token(ConstValues.INTG_TYPEID_VALUE, "var" + i);
            if (i < 2){
                token.setScope(ConstValues.GLOBAL_SCOPE);
            }
            if (i >= 2 && i < 6){
                token.setScope(ConstValues.MAIN_SCOPE);
            }
            else {
                token.setScope(j++);
            }
            tokens.add(token);
        }
    }

    public void insertTokens(){
        int size = tokens.size();
        for (int i = 0; i < size; i++){
            symbolTable.insertKey(tokens.get(i), tokens.get(i).getScope());
        }
    }

    public int howManyTokensExist(){
        int total = 0;
        int size = tokens.size();
        for (int i = 0; i < size; i++){
            Token token = tokens.get(i);
            int scope = symbolTable.existsKey(token.getName(), token.getScope());
            if (scope != SymbolTable.NOT_EXISTS){
                total++;
            }
        }
        return total;
    }

    @Test
    public void testGlobal(){
        symbolTable = new SymbolTable();

        initTokensGlobalAndMain();

        insertTokens();

        int total = howManyTokensExist();

        assertEquals(total, tokens.size());
    }

    @Test
    public void testGlobalAndMain(){
        symbolTable = new SymbolTable();

        initTokensGlobal();

        insertTokens();

        int total = howManyTokensExist();

        assertEquals(total, tokens.size());
    }

    @Test
    public void testMix(){
        symbolTable = new SymbolTable();

        initTokensMix();

        insertTokens();

        int total = howManyTokensExist();

        assertEquals(total, tokens.size());
    }

}