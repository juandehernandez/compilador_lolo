import org.junit.Test;

public class LoLoCTest {

    @Test
    public void FIBONACCI() {
        LLC compiler = new LLC("test_inputs/fibonacci.lolo", "test_outputs/fibonacci");
        compiler.compile();
    }

    @Test
    public void ifTEST() {
        LLC compiler = new LLC("test_inputs/if.lolo", "test_outputs/if");
        compiler.compile();
    }

    @Test
    public void whileTEST(){
        LLC compiler = new LLC("test_inputs/while.lolo", "test_outputs/while");
        compiler.compile();
    }

    @Test
    public void compile1() {
        LLC compiler = new LLC("test_inputs/test1.lolo", "test_outputs/test1");
        compiler.compile();
    }

    @Test
    public void compile2() {
        LLC compiler = new LLC("test_inputs/test2.lolo", "test_outputs/test2");
        compiler.compile();
    }

    @Test
    public void compile3() {
        LLC compiler = new LLC("test_inputs/test3.lolo", "test_outputs/test3");
        compiler.compile();
    }

    @Test
    public void compile4() {
        LLC compiler = new LLC("test_inputs/test4.lolo", "test_outputs/test4");
        compiler.compile();
    }

    @Test
    public void compile5() {
        LLC compiler = new LLC("test_inputs/test5.lolo", "test_outputs/test5");
        compiler.compile();
    }

    @Test
    /**
     * Tiene que fallar ya que hay una situación en el código no contemplada en el diseño del compilador
     * que es la resta (podría ser cualquier operación aritmética)
     */
    public void compile6() {
        LLC compiler = new LLC("test_inputs/test6.lolo", "test_outputs/test6");
        compiler.compile();
    }

    @Test
    public void compile7() {
        LLC compiler = new LLC("test_inputs/test7.lolo", "test_outputs/test7");
        compiler.compile();
    }

    @Test
    public void compile8() {
        LLC compiler = new LLC("test_inputs/test8.lolo", "test_outputs/test8");
        compiler.compile();
    }

    @Test
    public void compile9() {
        LLC compiler = new LLC("test_inputs/test9.lolo", "test_outputs/test9");
        compiler.compile();
    }

    @Test
    public void compileA() {
        LLC compiler = new LLC("test_inputs/testA.lolo", "test_outputs/testA");
        compiler.compile();
    }

    @Test
    public void compileB() {
        LLC compiler = new LLC("test_inputs/testB.lolo", "test_outputs/testB");
        compiler.compile();
    }

    @Test
    public void compileC() {
        LLC compiler = new LLC("test_inputs/testC.lolo", "test_outputs/testC");
        compiler.compile();
    }
}