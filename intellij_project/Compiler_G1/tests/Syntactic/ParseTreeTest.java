/*package Syntactic;

import IntermediateCode.IntermediateTable;
import Lexicon.ConstValues;
import Lexicon.Token;
import Symbols.SymbolTable;
import org.junit.Test;

import java.util.ArrayList;

public class ParseTreeTest {

    private Token[][] sentences = {
            //a = 3;
            {new Token(ConstValues.INTG_TYPEID_VALUE, "intg"), new Token(ConstValues.NAME, "a"), new Token(ConstValues.ASSIGN_EQUAL_VALUE, "="), new Token(ConstValues.INTG_TYPEID_VALUE, "3"), new Token(ConstValues.DELIM_VALUE, ";")},
            //ing a = 3;
            {new Token(ConstValues.INTG_TYPEID_VALUE, "intg"), new Token(ConstValues.NAME, "b"), new Token(ConstValues.ASSIGN_EQUAL_VALUE, "="), new Token(ConstValues.INTG_TYPEID_VALUE, "3"), new Token(ConstValues.DELIM_VALUE, ";")},
            //a = 3 + 5
            {new Token(ConstValues.NAME, "a"), new Token(ConstValues.ASSIGN_EQUAL_VALUE, "="), new Token(ConstValues.INTG_TYPEID_VALUE, "3"), new Token(ConstValues.MATH_OPERATION_VALUE, "+"), new Token(ConstValues.INTG_TYPEID_VALUE, "5"),new Token(ConstValues.DELIM_VALUE, ";")},
            //a = 3 + 5 + 7
            {new Token(ConstValues.NAME, "b"), new Token(ConstValues.ASSIGN_EQUAL_VALUE, "="), new Token(ConstValues.INTG_TYPEID_VALUE, "3"), new Token(ConstValues.MATH_OPERATION_VALUE, "+"), new Token(ConstValues.INTG_TYPEID_VALUE, "5"), new Token(ConstValues.MATH_OPERATION_VALUE, "+"), new Token(ConstValues.INTG_TYPEID_VALUE, "7"), new Token(ConstValues.DELIM_VALUE, ";")},
    };

    private ArrayList<Token>[] initSentences(){
        ArrayList<Token>[] sentenceList = new ArrayList[sentences.length];
        for(int i = 0; i < sentences.length; i++){
            sentenceList[i] = new ArrayList<>();
            for(int j = 0; j < sentences[i].length; j++){
                sentenceList[i].add(sentences[i][j]);
            }
        }
        return sentenceList;
    }

    @Test
    public void addSentence() {
        ArrayList<Token>[] sentenceList = initSentences();
        ParseTree tree = new ParseTree();

        tree.addSentence(sentenceList[0]);
    }

    @Test
    public void generateIntermediateCode() {
        ArrayList<Token>[] sentenceList = initSentences();
        ParseTree tree = new ParseTree();
        tree.addSentence(sentenceList[0]);
        tree.addSentence(sentenceList[1]);
        tree.addSentence(sentenceList[2]);
        tree.addSentence(sentenceList[3]);

        SymbolTable symbols = new SymbolTable();
        sentenceList[0].get(0).setType_id(ConstValues.INTG_VALUE);
        sentenceList[1].get(0).setType_id(ConstValues.INTG_VALUE);
        symbols.insertKey(sentenceList[0].get(0), -2);
        symbols.insertKey(sentenceList[1].get(0), -2);
        sentenceList[0].get(0).setScope(-2);
        sentenceList[1].get(0).setScope(-2);


        IntermediateTable table = tree.generateIntermediateCode(symbols);
        //System.out.println(table.toString());
    }
}
*/