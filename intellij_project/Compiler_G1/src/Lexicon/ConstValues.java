package Lexicon;

public abstract class ConstValues {
    // -------------- TOKENS ---------------------
    // Unknown
    public static final int UNKNOWN_TYPEID_VALUE = 0;

    // Number ( num )
    public static final int INTG_TYPEID_VALUE = 1;
    public static final int INTG_VALUE = 5;

    // Character (char)
    public static final int CHAR_TYPEID_VALUE = 2;
    public static final int CHAR_VALUE = 6;

    // String (str)
    public static final int STR_TYPEID_VALUE = 3;
    public static final int STR_VALUE = 7;

    // Boolean (bool)
    public static final int BOOL_TYPEID_VALUE = 4;
    public static final int BOOL_VALUE = 8;

    // Name of variables
    public static final int NAME = 9;

    // Sentence delimiter ( ; )
    public static final int DELIM_VALUE = 10;

    // Assignation equal ( = )
    public static final int ASSIGN_EQUAL_VALUE = 14;

    // Math operation ( + - * / % )
    public static final int MATH_OPERATION_VALUE = 16;

    // Opening parenthesis
    public static final int PARENTHESIS_OPENING = 17;
    public static final int PARENTHESIS_CLOSING = 18;

    //FUNCTION OPENING AND CLOSING
    public static final int FUNCTION_OPENING = 19;  // {
    public static final int FUNCTION_CLOSING = 20;  // }

    //END FUNCTIONS
    public static final int END_CODE = 21; // end

    //END LINE
    public static final int END_LINE = 22; // \r || \n

    //IF STATEMENT
    public static final int IF = 23; // if

    // WHILE STATEMENT
    public static final int WHILE = 24; // while

    //EQ value
    public static final int EQ = 30; // eq
    public static final int GT = 31; // gt
    public static final int LT = 32; // lt
    public static final int DIF = 33; // dif
    public static final int GTEQ = 34; // gteq
    public static final int LTEQ = 35; // lteq


    //-----------------------------Tipos de sentencia---------------------------------------------------------------//
    public static final int SENTENCE_TYPE_ASSIGNACIO = 1;
    public static final int SENTENCE_TYPE_OPERACIO = 2;

    // -------------- SCOPES ---------------------
    public static final int GLOBAL_SCOPE = -2;

    public static final int MAIN_SCOPE = -1;

    public static boolean isDatatype(int id){
        return (id == INTG_TYPEID_VALUE
                || id == CHAR_TYPEID_VALUE
                || id == STR_TYPEID_VALUE
                || id == BOOL_TYPEID_VALUE);
    }

    public static boolean isOperator(int id){
        return id == MATH_OPERATION_VALUE;
    }

    public static boolean abreParentesis(int id){return id == PARENTHESIS_OPENING;}

    public static boolean cierraParentesis(int id){return id == PARENTHESIS_CLOSING;}

    /**
     *
     * @param op1 operador 1
     * @param op2 operador 2
     * @return true si op1 es mas prioritario que o igual op2. false si no
     */
    public static boolean masPrioritario(Token op1, Token op2){
        int id1 = op1.getType_id(), id2 = op2.getType_id();

        if(id1 == MATH_OPERATION_VALUE){
            return true;
        }

        //Todo: comprobar la prioridad entre operaciones matematicas

        return id2 != MATH_OPERATION_VALUE;

    }

    public static boolean isLogicOperator(int id){
        return id == EQ || id == LT || id == GT || id == DIF || id == GTEQ || id == LTEQ;
    }

    public static int convertType(int type_id) {
        if (type_id == INTG_TYPEID_VALUE){
            return INTG_VALUE;
        }
        if (type_id == CHAR_TYPEID_VALUE){
            return CHAR_VALUE;
        }
        if (type_id == STR_TYPEID_VALUE){
            return STR_VALUE;
        }
        if (type_id == BOOL_TYPEID_VALUE){
            return BOOL_VALUE;
        }
        return -1; //En principio NUNCA debería entrar aquí
    }
}
