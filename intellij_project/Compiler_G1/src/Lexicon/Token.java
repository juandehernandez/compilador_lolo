package Lexicon;

public class Token {
    // Attributes
    private int type_id;
    private String name;
    private String file;
    private int fileLine;
    private int scope;

    // Constructor
    public Token(int type_id, String name) {
        this.type_id = type_id;
        this.name = name;
    }

    // Getters & Setters
    public int getType_id() {
        return type_id;
    }
    public String getName() {
        return name;
    }
    public String getFile() {
        return file;
    }
    public int getFileLine() {
        return fileLine;
    }
    public void setType_id(int type_id) {
        this.type_id = type_id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setFile(String file) {
        this.file = file;
    }
    public void setFileLine(int fileLine) {
        this.fileLine = fileLine;
    }
    public int getScope() {
        return scope;
    }
    public void setScope(int scope) {
        this.scope = scope;
    }

    // Functions
    public boolean isVariable(){
        return getType_id() == ConstValues.NAME;
    }
}
