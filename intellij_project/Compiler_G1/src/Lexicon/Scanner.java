package Lexicon;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import Error.Error;
import Error.ErrorHandler;
import Error.LexicError;

public class Scanner{

    private static String FILENAME;

    private FileInputStream fileInputStream;
    private int scope;
    private static int linea;
    private ArrayList<Token> tokens;
    private ErrorHandler errorHandler;

    // Constructor
    public Scanner(ErrorHandler errorHandler, String sourceFile) throws FileNotFoundException {

        Scanner.FILENAME = sourceFile;

        linea = 1;
        scope = ConstValues.GLOBAL_SCOPE;
        this.errorHandler = errorHandler;
        tokens = new ArrayList<Token>();
        File file = new File(FILENAME);
        if (!file.exists()){
            //System.out.println("L'arxiu no existeix!");
            throw new FileNotFoundException("No existeix l'arxiu");
        }else {
            fileInputStream = new FileInputStream(file);
        }
    }




    // Functions and Procedure
    public Token nextToken() throws IOException, Error.ErrorTypeException {

        if (tokens.size() == 0){
            char c = '-'; // we will read the file char by char
            StringBuilder stringBuilder = new StringBuilder();
            Token token = null;

            // while file is not readen at all or we don't have created any token
            while (fileInputStream.available() > 0){
                // get character
                c = (char) fileInputStream.read();

                if (c == ';'){
                    // sentences delimiters
                    String sentence = stringBuilder.toString();
                    stringBuilder = new StringBuilder();

                    if (sentence.length() > 0){
                        // Block before ;
                        token = TokensKnown.checkToken(sentence);
                        token.setFileLine(getLinea());
                        token.setFile(FILENAME);
                        token.setScope(scope);

                        //outQueue.put(token);
                        tokens.add(token);
                    }

                    // -- TOKEN of ';'
                    stringBuilder.append(c);
                    sentence = stringBuilder.toString();
                    stringBuilder = new StringBuilder();

                    token = TokensKnown.checkToken(sentence);
                    token.setFileLine(getLinea());
                    token.setFile(FILENAME);
                    token.setScope(scope);

                    //outQueue.put(token);
                    tokens.add(token);
                    break;

                }else if (c == ' ' || c == '\n' || c == '\r'){
                    // new block OR multi spaces OR new line
                    if (stringBuilder.toString().length() != 0){
                        // new instruction
                        String sentence = stringBuilder.toString();
                        stringBuilder = new StringBuilder();

                        token = TokensKnown.checkToken(sentence);
                        token.setFileLine(getLinea());
                        token.setFile(FILENAME);
                        token.setScope(scope);

                        //outQueue.put(token);
                        tokens.add(token);
                    }

                    if (c == '\n'){
                       // System.out.println("Linea numero " + linea);
                        linea++;
                    }


                }else if (c == '+' || c == '-' || c == '*' || c == '/' || c == '%' || c == '(' || c == ')'){
                    String sentence = stringBuilder.toString();

                    // Previous TOKEN (sentence)
                    if (sentence.length() > 0){
                        // new instruction
                        stringBuilder = new StringBuilder();

                        token = TokensKnown.checkToken(sentence);
                        token.setFileLine(getLinea());
                        token.setFile(FILENAME);
                        token.setScope(scope);

                        //outQueue.put(token);
                        tokens.add(token);
                    }

                    // TOKEN of the Mathematic operation
                    stringBuilder.append(c);
                    sentence = stringBuilder.toString();
                    stringBuilder = new StringBuilder();

                    token = TokensKnown.checkToken(sentence);
                    token.setFileLine(getLinea());
                    token.setFile(FILENAME);
                    token.setScope(scope);

                    //outQueue.put(token);
                    tokens.add(token);
                    break;

                }else {
                    // we still in construction
                    stringBuilder.append(c);
                }
            }

            if (stringBuilder.toString().length() > 0){
                // Hemos ido leyendo pero encontramos el final del fichero
                token = TokensKnown.checkToken(stringBuilder.toString());
                token.setFileLine(getLinea());
                token.setFile(FILENAME);
                token.setScope(scope);

                //outQueue.put(token);
                tokens.add(token);
            }
        }

        if (tokens.isEmpty()){
            return null;
        }

        Token token = tokens.get(0);
        tokens.remove(0);

        if (token.getType_id() == 0){
            errorHandler.addError(new LexicError(token.getFileLine(), token.getFile(), 0));
        }
        return token;
    }


    private boolean ilegalCharcater(char caracter){
        return (caracter == '!' || caracter == '*' || caracter == '+' || caracter == '\\' ||
                caracter == '"' || caracter == '<' || caracter == '#' || caracter == '(' ||
                caracter == '=' || caracter == '|' || caracter == '{' || caracter == '>' ||
                caracter == '%' || caracter == ')' || caracter == '~' || caracter == ';' ||
                caracter == '}' || caracter == '/' || caracter == '^' || caracter == '[' ||
                caracter == ':' || caracter == ',' || caracter == '?' || caracter == ']' ||
                caracter == '\'' || caracter == '.' || caracter == '&' || caracter == '\n' ||
                caracter == '\r' || caracter == '-');
    }

    public static String getFILENAME() {
        return FILENAME;
    }

    public static int getLinea() { return linea; }
}
