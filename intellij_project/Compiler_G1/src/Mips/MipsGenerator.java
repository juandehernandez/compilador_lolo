package Mips;

import IntermediateCode.IntermediateTable;
import IntermediateCode.IntermediateTableEntry;
import Lexicon.ConstValues;
import Lexicon.Token;
import Symbols.SymbolTable;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

/**
 * Esta clase se encarga de generar el código mips gracias a una tabla de código intermedio y una tabla de símbolos.
 */
public class MipsGenerator {
    // Attributes
    private IntermediateTable intermediateTable;
    private SymbolTable symbolTable;
    public String mips;

    private String outputCode;
    //Esta variable offsetRegisters se usa para saber en que offset desde el puntero de fragmento se ha guardado una variable.
    //De esta manera cuando se quiera usar su valor, cargaremos mediante código mips la variable en un registro (offset($fp)).
    private HashMap<String, Integer> offsetRegisters;
    //private int indexOffset; //TODO: do it for data types size (int = 4 Bytes, char = 1 Byte)
    //Esta variable fp se usa para saber en que offset desde el puntero de fragmento hay que guardar un valor.
    private int fp; //Fragment pointer
    //Esta variable es una LIFO (Last in first out) para los tags de un IF y WHILE statements
    private Stack tags;
    //Timestamp para ids únicos para etiquetas de la lifo
    private int timestamp;

    //private String mips = "";

    /**
     * Constructor
     * @param intermediateTable Tabla de código intermedio. Para cada entrada de esta tabla, generamos un pequeño código mips.
     * @param symbolTable Tabla de símbolos útil para comprobar si una variable existe en la tabla de símbolos, obtener su valor y en qué scope se encuentra.
     */
    public MipsGenerator(IntermediateTable intermediateTable, SymbolTable symbolTable){
        this.intermediateTable = intermediateTable;
        this.symbolTable = symbolTable;

        //Es
        offsetRegisters = new HashMap<String, Integer>();
        fp = -4;

        //Inicializamos la LIFO de los tags
        tags = new Stack();
        //Inicializamos Timestamp for unique IDS for LIFO
        timestamp = 1;

        this.mips = "";

        //buildRegisters();
        /*System.out.println("Intermediate table entries are:");
        for (int i = 0; i < intermediateTable.size(); i++) {
            System.out.println(intermediateTable.getEntries().get(i).getOP1().getName() + " " + intermediateTable.getEntries().get(i).getOp().getName() + " " + intermediateTable.getEntries().get(i).getOP2().getName());
        }*/

    }

    // Offset functions for registers
    /*private void buildRegisters(){
        offsetRegisters = new HashMap<String, Integer>();
        indexOffset = 0;
        int totalEntries = intermediateTable.size();

        for (int i = 0; i < totalEntries; i++){
            IntermediateTableEntry entry = intermediateTable.getEntries().get(i);

            introduceRegister(entry.getOP1());
            introduceRegister(entry.getOP2());
            introduceRegister(entry.getR());
        }
    }

    private void introduceRegister(Token token) {
        // Miramos si se trata de un número o de una variable/registro
        try{
            int num = Integer.parseInt(token.getName());

            // Es un número, no hacemos nada más (no salta exception)

        }catch (NumberFormatException e) {
            // Es una variable o un nuevo registro creado por la IntermediateTable

            if (offsetRegisters.get(token.getName()) != null){
                // No existe su entrada en la tabla
                offsetRegisters.put(token.getName(), indexOffset);

                indexOffset++;// TODO: Cambiar por el tamaño del tipo de datos del que se trata (int, char)
            }

        }


    }*/

    /**
     *
     */
    private int ifstatement (ArrayList<IntermediateTableEntry> entries, int i) {

        String tag = "L" + timestamp++;
        tags.push(tag);

        String mipsCode = "";

        int initial = Integer.parseInt(entries.get(i).getOP2().getName());
        while (initial < i){
            initial = recursiveMips(entries, initial);
        }

        mipsCode = mipsCode + "beq   $t2, $zero, " + tag;

        int ultimaFila = Integer.parseInt(entries.get(i).getR().getName());
        int index = i + 1;
        while (index < ultimaFila) {
            System.out.println("Llamada rec if");
            index = recursiveMips(entries, index);
        }

        mipsCode = mipsCode + tags.pop() + ":\n";

        this.mips = this.mips + mipsCode;

        return ultimaFila;
    }

    private int whilestatement (ArrayList<IntermediateTableEntry> entries, int i) {

        String mipsCode = "";

        String tagComp = "L" + timestamp++;
        this.mips = this.mips + "b   " + tagComp + "\n";
        tags.push(tagComp);


        String condition = tagComp + ":\n";
        int initial = Integer.parseInt(entries.get(i).getOP2().getName());
        while (initial < i){
            initial = recursiveMips(entries, initial);
        }

        tags.pop();

        String tagBloque = "L" + timestamp++ + ":\n";
        condition = condition + lt(entries, i - 1) + "beq   $t2, $zero, " + tagBloque;

        tags.push(tagBloque);
        this.mips = this.mips + tagBloque;
        String bloque = "";
        int ultimaFila = Integer.parseInt(entries.get(i).getR().getName());
        int index = i + 1;
        while (index < ultimaFila) {
            index = recursiveMips(entries, index);
        }

        tags.pop();

        //mips = mips + tags.pop() + ":\n";

        mipsCode = mipsCode + bloque + condition;

        this.mips = this.mips + mipsCode;
        return ultimaFila;
    }

    /*private String goodSuma(ArrayList<IntermediateTableEntry> entries, int i){
        String mips = "";
        IntermediateTableEntry entry = entries.get(i);
        String name1 = entry.getOP1().getName();
        String name2 = entry.getOP2().getName();
        boolean exists = false;
        int valorResultado;
        int valor1, valor2;
        int tipo1, tipo2;

        // dos variables
        int scope1 = symbolTable.existsKey(name1, -2);
        if ( scope1 == -2){
            // Variable (1)
            tipo1 = 1;
            valor1 = offsetRegisters.get(name1);
        }else if (scope1 == -5){
            // registro
            tipo1 = 2;
            valor1 = Integer.parseInt(symbolTable.getSymbol(name1, -5).getValue());
        }else{
            // num
            tipo1 = 3;
            valor1 = Integer.parseInt(entry.getOP1().getName());
        }

        int scope2 = symbolTable.existsKey(name2, -2);
        if ( scope2 == -2){
            // Variable (1)
            tipo2 = 1;
            valor2 = offsetRegisters.get(name2);
        }else if (scope2 == -5){
            // registro
            tipo2 = 2;
            valor2 = Integer.parseInt(symbolTable.getSymbol(name2, -5).getValue());
        }else{
            // num
            tipo2 = 3;
            valor2 = Integer.parseInt(entry.getOP2().getName());
        }
        // 1 var 1 num
        // 1 num 1 var
        // 2 nums
    // 2 R
        // 1 R 1 var
        // 1 var 1 R
        // 1 R 1 num
        // ! num 1 R


        StringBuilder stringBuilder = new StringBuilder();
        switch (tipo1){
            case 1:
                switch (tipo2){
                    case 1:
                        //Primero cargamos los dos valores de los offset en la stack donde se encuentran las variables
                        //Así tendremos en registro t2 el valor de la variable operando 1 y en el registro t3 el del operando 2.
                        stringBuilder.append("LW   $t2, " + valor1 + "($fp)\n"
                                + "LW   $t3, " + valor2 + "($fp)\n");
                        //Seguidamente los sumamos
                        stringBuilder.append("ADDU   $t2, $t2, $t3");
                        valorResultado = Integer.parseInt(symbolTable.getSymbol(name1, -2).getValue()) + Integer.parseInt(symbolTable.getSymbol(name2, -2).getValue());
                        break;
                    case 2:
                        //Cargamos el valor de la variable operando 1 en el registro t2 para despues sumarlo con el valor del operando 2 que directamente es un número.
                        stringBuilder.append("LW   $t2, " + valor1 + "($fp)\n"
                                + "ADDIU   $t2, $t2, " + valor2 + "\n");
                        valorResultado = Integer.parseInt(symbolTable.getSymbol(name1, -2).getValue()) + Integer.parseInt(symbolTable.getSymbol(name2, -5).getValue());
                        break;
                    default:
                        //Añadimos en registro t2 el valor del operando 1 (sumando con 0, así es la manera de hacerlo).
                        stringBuilder.append("LW   $t2, " + valor1 + "($fp)\n");
                        //Lo mismo para el operando 2 en el registro t3.
                        stringBuilder.append("ADDIU   $t3, $zero, " + valor2 + "\n");
                        //Ahora ya se pueden sumar los valores que estan en los registros t2 y t3, poniendo el resultado en el registro t2.
                        stringBuilder.append("ADDU   $t2, $t2, $t3\n");
                        valorResultado = valor1 + valor2;
                        break;
                }
                break;
            case 2:
                switch (tipo2){
                    case 1:
                        stringBuilder.append("LW   $t2, " + valor2 + "($fp)\n"
                                + "ADDIU   $t2, $t2, " + valor1 + "\n");
                        valorResultado = Integer.parseInt(symbolTable.getSymbol(name2, -2).getValue()) + Integer.parseInt(symbolTable.getSymbol(name1, -5).getValue());
                        break;
                    case 2:

                        //Añadimos en registro t2 el valor del operando 1 (sumando con 0, así es la manera de hacerlo).
                        stringBuilder.append("ADDIU   $t2, $zero, " + valor1 + "\n");
                        //Lo mismo para el operando 2 en el registro t3.
                        stringBuilder.append("ADDIU   $t3, $zero, " + valor2 + "\n");
                        //Ahora ya se pueden sumar los valores que estan en los registros t2 y t3, poniendo el resultado en el registro t2.
                        stringBuilder.append("ADDU   $t2, $t2, $t3\n");

                        valorResultado = valor1 + valor2;

                        break;
                    default:
                        break;
                }
                break;
            default:
                switch (tipo2){
                    case 1:
                        break;
                    case 2:
                        break;
                    default:
                        break;
                }
                break;
        }


        if (symbolTable.existsKey(entry.getR().getName(), SymbolTable.MIPS_AUX) == SymbolTable.NOT_EXISTS){
            symbolTable.insertKey(new Token(ConstValues.INTG_VALUE, entry.getR().getName()), SymbolTable.MIPS_AUX);
            symbolTable.getSymbol(entry.getR().getName(), SymbolTable.MIPS_AUX).setValue(String.valueOf(valorResultado));
        }
        return null;
    }*/
    private String lt(ArrayList<IntermediateTableEntry> entries, int i){
        int offset = 0, offset2 = 0;
        StringBuilder stringBuilder = new StringBuilder();
        //En el primer caso comprovamos si los dos operandos son variables
        if (symbolTable.existsKey(entries.get(i).getOP1().getName(), -2) == -2
                && symbolTable.existsKey(entries.get(i).getOP2().getName(), -2) == -2) {

            //Obtenemos los offset en mips de las variables a comparar
            offset = offsetRegisters.get(entries.get(i).getOP1().getName());
            offset2 = offsetRegisters.get(entries.get(i).getOP2().getName());
            //Primero cargamos los dos valores del offset en memoria donde se encuentren
            stringBuilder.append("LW    $t2, " + offset + "($fp)\n"
                    + "LW   $t3, " + offset2 + "($fp)\n");
            //Seguidamente los restamos
            stringBuilder.append("SLT   $t2, $t2, $t3\n");

        } else if (symbolTable.existsKey(entries.get(i).getOP1().getName(), -2) == -2) {
            //Si el operando 1 existe en la tabla de simbolos miramos su offset
            offset = offsetRegisters.get(entries.get(i).getOP1().getName());
            int value = Integer.parseInt(entries.get(i).getOP2().getName());
            stringBuilder.append("LW    $t2, " + offset + "($fp)\n"
                    + "SLT    $t2, $t2, " + value + "\n");

        } else if (symbolTable.existsKey(entries.get(i).getOP2().getName(), -2) == -2) {
            //Si el operando 1 no existe, será operando 2 al que tendremos que mirar su offset (si se ha aplicado optimización)
            offset = offsetRegisters.get(entries.get(i).getOP2().getName());
            int value = Integer.parseInt(entries.get(i).getOP1().getName());

            stringBuilder.append("LW    $2, " + offset + "($fp)\n"
                    + "SLT  $t2, $t2, " + (value + 1) + "\n");

            //Es posible que tengamos el operando 1 en formato registro (acumulado anteriormente).
            //Entonces actuamos de la siguiente manera sabiendo que el valor está en el registro t2
        } else {
            //En este caso entraremos cuando la operación sea entre dos numeros (ninguna variable)
            int value1 = Integer.parseInt(entries.get(i).getOP1().getName());
            int value2 = Integer.parseInt(entries.get(i).getOP2().getName());

            stringBuilder.append("ADDIU   $t2, $zero, " + value1 + "\n");
            stringBuilder.append("SLT   $t2, $t2, " + value2 + "\n");
        }

        //this.mips = this.mips + stringBuilder.toString();
        //return i + 1;
        return stringBuilder.toString();
    }

    private int suma(ArrayList<IntermediateTableEntry> entries, int i) {
        int offset = 0;
        int offset2 = 0;
        int value = 0;
        StringBuilder stringBuilder = new StringBuilder();
        //Miraremos cual es la variable a sumar para buscar su offset (para el fragment pointer)
        //En el primer caso comprovamos si los dos operandos son variables
        if (symbolTable.existsKey(entries.get(i).getOP1().getName(), -2) == -2
                && symbolTable.existsKey(entries.get(i).getOP2().getName(), -2) == -2) {

            //Obtenemos el offset del operando 1
            offset = offsetRegisters.get(entries.get(i).getOP1().getName());
            //Obtenemos el offset del operando 2
            offset2 = offsetRegisters.get(entries.get(i).getOP2().getName());

            //Primero cargamos los dos valores de los offset en la stack donde se encuentran las variables
            //Así tendremos en registro t2 el valor de la variable operando 1 y en el registro t3 el del operando 2.
            stringBuilder.append("LW   $t2, " + offset + "($fp)\n"
                    + "LW   $t3, " + offset2 + "($fp)\n");
            //Seguidamente los sumamos
            stringBuilder.append("ADDU   $t2, $t2, $t3");


        } else if (symbolTable.existsKey(entries.get(i).getOP1().getName(), -2) == -2) {
            //Si el operando 1 existe en la tabla de simbolos miramos su offset, es decir, en qué fragmento se encuentra
            offset = offsetRegisters.get(entries.get(i).getOP1().getName());
            value = Integer.parseInt(entries.get(i).getOP2().getName());
            //Cargamos el valor de la variable operando 1 en el registro t2 para despues sumarlo con el valor del operando 2 que directamente es un número.
            stringBuilder.append("LW   $t2, " + offset + "($fp)\n"
                    + "ADDIU   $t2, $t2, " + value + "\n");


        } else if (symbolTable.existsKey(entries.get(i).getOP2().getName(), -2) == -2) {
            //Si el operando 1 no existe, miramos si existe operando 2 al que tendremos que mirar su offset, es decir, en que fragmento se encuentra
            if (offsetRegisters.containsKey(entries.get(i).getOP2().getName())) {
                offset = offsetRegisters.get(entries.get(i).getOP2().getName());
                value = Integer.parseInt(entries.get(i).getOP1().getName());
            }
            //Cargamos el valor de la variable operando 2 en el registro t2 para despues sumarlo con el valor del operando 1 que directamente es un número.
            stringBuilder.append("LW   $t2, " + offset + "($fp)\n"
                    + "ADDIU   $t2, $t2, " + value + "\n");


        } else if (entries.get(i).getOP1().getName().charAt(0) != 'R'
                && entries.get(i).getOP2().getName().charAt(0) != 'R') {
            //Si no hay variable en la operación, es porque son dos números que se quieren sumar, entonces resolvemos
            int value1 = Integer.parseInt(entries.get(i).getOP1().getName());
            int value2 = Integer.parseInt(entries.get(i).getOP2().getName());
            //Añadimos en registro t2 el valor del operando 1 (sumando con 0, así es la manera de hacerlo).
            stringBuilder.append("ADDIU   $t2, $zero, " + value1 + "\n");
            //Lo mismo para el operando 2 en el registro t3.
            stringBuilder.append("ADDIU   $t3, $zero, " + value2 + "\n");
            //Ahora ya se pueden sumar los valores que estan en los registros t2 y t3, poniendo el resultado en el registro t2.
            stringBuilder.append("ADDU   $t2, $t2, $t3\n");


        } else {
            //En este caso el valor viene acumulado así que se encuentra en el registro temporal t2
            if (symbolTable.existsKey(entries.get(i).getOP2().getName(), -2) == -2) {
                //Si existe variable operando 2
                stringBuilder.append("ADDIU   $t2, $t2, "
                        + symbolTable.getSymbol(entries.get(i).getOP2().getName(), -2).getValue() + "\n");

            } else {
                //Si es un número directamente
                stringBuilder.append("ADDIU   $t2, $t2, " + entries.get(i).getOP2().getName() + "\n");

            }
        }

        this.mips = this.mips + stringBuilder.toString();
        return i + 1;
    }

    public int resta(ArrayList<IntermediateTableEntry> entries, int i) {
        StringBuilder stringBuilder = new StringBuilder();
        int offset = 0, offset2 = 0;

        boolean numeric1 = true, numeric2 = true;
        try {
            Integer num = Integer.parseInt(entries.get(i).getOP1().getName());
        } catch (NumberFormatException e) {
            numeric1 = false;
        }
        try {
            Integer num = Integer.parseInt(entries.get(i).getOP2().getName());
        } catch (NumberFormatException e) {
            numeric2 = false;
        }



//Miramos cual es la variable a restar para buscar su offset (para el fragment pointer)
        //En el primer caso comprovamos si los dos operandos son variables
        if (symbolTable.existsKey(entries.get(i).getOP1().getName(), -2) == -2
                && symbolTable.existsKey(entries.get(i).getOP2().getName(), -2) == -2) {

            offset = offsetRegisters.get(entries.get(i).getOP1().getName());
            offset2 = offsetRegisters.get(entries.get(i).getOP2().getName());
            //Primero cargamos los dos valores del offset en memoria donde se encuentren
            stringBuilder.append("LW   $t2, " + offset + "($fp)\n"
                    + "LW   $t3, " + offset2 + "($fp)\n");
            //Seguidamente los restamos
            stringBuilder.append("SUBU   $t2, $t3, $t2");

        } else if (symbolTable.existsKey(entries.get(i).getOP1().getName(), -2) == -2) {
            //Si el operando 1 existe en la tabla de simbolos miramos su offset
            offset = offsetRegisters.get(entries.get(i).getOP1().getName());
            int value = Integer.parseInt(entries.get(i).getOP2().getName());
            stringBuilder.append("ADDIU   $t3, $zero, " + value + "\n"
                    + "LW   $t2, " + offset + "($fp)\n"
                    + "SUBU $t2, $t3, $t2\n");

        } else if (symbolTable.existsKey(entries.get(i).getOP2().getName(), -2) == -2) {
            //Si el operando 1 no existe, miramos si existe operando 2 al que tendremos que mirar su offset (si se ha aplicado optimización)
            if (offsetRegisters.containsKey(entries.get(i).getOP2().getName())) {
                offset = offsetRegisters.get(entries.get(i).getOP2().getName());
                int value = Integer.parseInt(entries.get(i).getOP1().getName());

                stringBuilder.append("LW $t2, " + offset + "\n"
                        + "ADDIU $t2, $t2, -" + value + "\n");
            }

        } else if (numeric1 && numeric2) {
            //Si no hay variable en la operación, es porque son dos numeros que se quieren restar, entonces resolvemos
            int value1 = Integer.parseInt(entries.get(i).getOP1().getName());
            int value2 = Integer.parseInt(entries.get(i).getOP2().getName());
            stringBuilder.append("ADDIU   $t2, $zero, " + value1 + "\n");
            stringBuilder.append("ADDIU   $t3, $zero, " + value2 + "\n");
            stringBuilder.append("SUBU   $t2, $t3, $t2\n");

        } else {
            //En este caso el valor viene acumulado así que se encuentra en el registro temporal t2
            if (symbolTable.existsKey(entries.get(i).getOP2().getName(), -2) == -2) {
                stringBuilder.append("ADDIU   $t3, $zero, "
                        + symbolTable.getSymbol(entries.get(i).getOP2().getName(), -2).getValue() + "\n");
            } else {
                stringBuilder.append("ADDIU   $t3, $zero, "
                        + entries.get(i).getOP2().getName() + "\n");

            }
            stringBuilder.append("SUBU   $t2, $t3, $t2\n");
        }

        this.mips = this.mips + stringBuilder.toString();
        return i + 1;

    }

    public int multiplica(ArrayList<IntermediateTableEntry> entries, int i) {
        StringBuilder stringBuilder = new StringBuilder();
        boolean numeric1 = true, numeric2 = true;
        try {
            Integer num = Integer.parseInt(entries.get(i).getOP1().getName());
        } catch (NumberFormatException e) {
            numeric1 = false;
        }
        try {
            Integer num = Integer.parseInt(entries.get(i).getOP2().getName());
        } catch (NumberFormatException e) {
            numeric2 = false;
        }

        int offset = 0;
        //El caso de la multiplicación es diferente a las otras operaciones.
        //El método usado es poner zeros por el lado de menos peso para multiplicar por dos cuantos se puedan.
        //Cuando ya no hay que poner más zeros, tenemos un valor resultante que es el que se suma para dar el resultado correcto.
        //Es decir 3x3 = 9. Entonces haremos un shift al valor 3 (poner un 0 lsb) y el valor será 6,
        // que ya no puede ser multiplicado por dos porqué pasaria a ser 12, entonces se sumaria 3.
        //Entonces serian un shift y una suma con 3.

        //Variable para saber cual es el numero al que hacemos shift left logical (poner zeros lsb)
        int num = 0;
        //Variable para contar los shifts que habrá que hacerle al num.
        int shifts = -1;

        //Variable para saber si es el caso que la operación es entre dos números (ninguna variable)
        boolean noVarsInOperation = false;

        int resultat = 0;

        //En el primer caso comprovamos si los dos operandos son variables
        if (symbolTable.existsKey(entries.get(i).getOP1().getName(), -2) == -2
                && symbolTable.existsKey(entries.get(i).getOP2().getName(), -2) == -2) {

            offset = offsetRegisters.get(entries.get(i).getOP1().getName());
            num = Integer.parseInt(symbolTable.getSymbol(entries.get(i).getOP1().getName(), -2).getValue());
            resultat = num * Integer.parseInt(symbolTable.getSymbol(entries.get(i).getOP2().getName(), -2).getValue());



        } else if (symbolTable.existsKey(entries.get(i).getOP1().getName(), -2) == -2) {
            //Si el operando 1 existe en la tabla de simbolos miramos su offset
            offset = offsetRegisters.get(entries.get(i).getOP1().getName());
            num = Integer.parseInt(symbolTable.getSymbol(entries.get(i).getOP1().getName(), -2).getValue());
            resultat = num * Integer.parseInt(entries.get(i).getOP2().getName());


        } else if (symbolTable.existsKey(entries.get(i).getOP2().getName(), -2) == -2) {
            //Si el operando 1 no existe, será operando 2 al que tendremos que mirar su offset (si se ha aplicado optimización)
            System.out.println("Mips code is:\n" + stringBuilder.toString());
            offset = offsetRegisters.get(entries.get(i).getOP2().getName());
            num = Integer.parseInt(symbolTable.getSymbol(entries.get(i).getOP2().getName(), -2).getValue());
            resultat = num * Integer.parseInt(entries.get(i).getOP1().getName());

            //Es posible que tengamos el operando 1 en formato registro (acumulado anteriormente).
            //Entonces actuamos de la siguiente manera sabiendo que el valor está en el registro t2
        } else if (symbolTable.existsKey(entries.get(i).getOP1().getName(), -5) == -5) {
            num = Integer.parseInt(symbolTable.getSymbol(entries.get(i).getOP1().getName(), -5).getValue());

            if (symbolTable.existsKey(entries.get(i).getOP2().getName(), -2) == -2) {
                resultat = num * Integer.parseInt(symbolTable.getSymbol(entries.get(i).getOP2().getName(), -2).getValue());
            } else {
                resultat = num * Integer.parseInt(entries.get(i).getOP2().getName());
            }
        } else {
            //En este caso entraremos cuando la operación sea entre dos numeros (ninguna variable)
            noVarsInOperation = true;

            num = Integer.parseInt(entries.get(i).getOP1().getName());
            resultat = num * Integer.parseInt(entries.get(i).getOP2().getName());
        }

        //TODO Habrá que mirar si el valor es negativo para todas las operaciones
                    /*if (resultat < 0) {
                        resultat = Math.abs(resultat);
                    }*/

        int aux = num;
        //Lo que queremos conseguir con el bucle es saber cuantos shifts (añadir zeros LSB (multiplicación por dos))
        // hay que hacer y luego sumar restante para conseguir el resultado que queremos
        while(aux <= resultat) {
            aux = aux * 2;
            shifts++;
        }

        if (noVarsInOperation) {
            stringBuilder.append("ADDIU   $t2, $zero, " + entries.get(i).getOP1().getName() + "\n");
        } else if (numeric1 && numeric2) {
            stringBuilder.append("LW   $t2, " + offset + "($fp)\n");
        }
        //Si el resultado es el numero será porque se ha multiplicado por uno,
        // entonces no hace falta hacer ni shift ni addiu, directamente cargar el valor de la variable en el registro
        if (resultat != num) {
            int restant = resultat - (aux / 2);
            stringBuilder.append("SLL   $t2, $t2, " + shifts + "\n");
            if (restant != 0) {
                stringBuilder.append("ADDIU   $t2, $t2, " + restant + "\n");

            }
        }

        this.mips = this.mips + stringBuilder.toString();
        return i + 1;
    }

    /**
     *
     */
    public int recursiveMips(ArrayList<IntermediateTableEntry> entries, int i) {
        if (i < entries.size()){
            switch (entries.get(i).getOp().getName()) {
                case "+":
                    i = suma(entries, i);
                    break;
                case "-":
                    //mips = mips + resta();
                    i = resta(entries, i);
                    break;
                case "*":
                    i = multiplica(entries, i);
                    break;
                case "if":
                    tags.push("L" + timestamp);
                    i = ifstatement(entries, i);
                    break;
                case "initwhile":
                    i = whilestatement(entries, i);
                    break;
                case "=":
                    i = equal(entries, i);
                    break;
                default:
                    i++;
                    break;

            }
        }else {
            i = 100;
        }

        return i;
    }

    /**
     * Función que genera el código mips recorriendo todas las entradas de la tabla de código intermedio.
     * Para cada entrada, tenemos dos operandos y una operación, que se transforman en un pequeño código mips y se va guardando en un StringBuilder.
     */

    public int equal(ArrayList<IntermediateTableEntry> entries, int i){
        boolean numeric1 = true;
        try {
            Integer num = Integer.parseInt(entries.get(i).getOP1().getName());
        } catch (NumberFormatException e) {
            numeric1 = false;
        }

        StringBuilder stringBuilder = new StringBuilder();
        if (symbolTable.existsKey(entries.get(i).getOP1().getName(), -2) == -2){
            //En este caso entraremos si se quiere asignar un valor de una variable.
            //Cogeremos el valor de la variable y lo añadiremos a la dirección de la nueva variable también.
            int varOffset = offsetRegisters.get(entries.get(i).getOP1().getName());
            stringBuilder.append("LW   $t2, " + varOffset + "($fp)\n");
        } else if (symbolTable.existsKey(entries.get(i).getOP1().getName(), -5) == -5) {

            int valor = Integer.parseInt(symbolTable.getSymbol(entries.get(i).getOP1().getName(), -5).getValue());
            stringBuilder.append("ADDIU   $t2, $zero, " + valor + "\n");
        } else if (numeric1){

            stringBuilder.append("ADDIU   $t2, $zero, " + entries.get(i).getOP1().getName() + "\n");
        }

        if (offsetRegisters.containsKey(entries.get(i).getOP2().getName())) {

            stringBuilder.append("SW   $t2, " + offsetRegisters.get(entries.get(i).getOP2().getName()) + "($fp)\n");

        } else {
            //Ponemos en el hashmap el valor del offset desde el fragment pointer donde se encontrará una variable en memoria.
            offsetRegisters.put(entries.get(i).getOP2().getName(), fp);
            stringBuilder.append("SW   $t2, " + fp + "($fp)\n");
            //Obtenemos el número de bytes que necesita para ser guardado el tipo de dato (char = 1 byte; intg = 4 bytes...)
            int bytes = numBytes(entries.get(i).getOP2());
            fp = fp - bytes;
        }


        this.mips = this.mips + stringBuilder.toString();
        return i + 1;
        //Como se ha hecho asignación, añadimos el valor en la tabla de símbolos.
        //if (symbolTable.existsKey(entries.get(i).getR().getName(), SymbolTable.MIPS_AUX) == SymbolTable.NOT_EXISTS){
         //   symbolTable.insertKey(new Token(ConstValues.INTG_VALUE, entries.get(i).getR().getName()), SymbolTable.MIPS_AUX);
          //  symbolTable.getSymbol(entries.get(i).getR().getName(), SymbolTable.MIPS_AUX).setValue(String.valueOf(valorResultado));
        //}
        //symbolTable.getSymbol(entries.get(i).getOP2().getName(), -2).setValue(entries.get(i).getOP1().getName());
    }
    public void generateMipsCode() {
        //Entradas de la tabla de código intermedio.
        ArrayList<IntermediateTableEntry> entries = intermediateTable.getEntries();
        //Variable para ir acumulando el código mips.
        StringBuilder stringBuilder = new StringBuilder();

        //Último resultado de una operación. Lo guardamos para la multiplicación
        // porque para saber cuantos shifts hay que hacerle a un número habrá que saber qué resultado ha dado la operación anterior
        // También para las demás operaciones, saber que valor habrá acumulado en mips en ese momento en el registro t2
        int lastResult = 0;
        boolean valueAcumulated = false;

        //Variable para saber si al encontrar un operador if se hará un BEQ o un BNE
        //BEQ = Branch jump if equal; BNE = Branch jump if not equal
        //Esto es porque siempre hacemos un slt (set if less than) para las comparaciones,
        // y entonces la condición para saltar del if habrá variado
        boolean doBEQ = true;

        //Variable para saber cuantas sentencias hay en un bloque de while para cuando llega un endwhile formar bien los bloques de mips (los jumps)
        ArrayList<Integer> countSentences = new ArrayList<>();
        int countEnds = 0;

        for (int i = 0; i < entries.size(); i++) {
            //Inicializamos para que no haya problemas a 0 mismo. Estas variables se usan como auxiliares
            // para obtener un offset de una variable (donde se habia guardado (offset desde el fragment pointer))
            int offset = 0;
            int offset2 = 0;
            //Variable que se usa como auxiliar para obtener el valor de un operando en formato int (el operando está en string por defecto).
            int value = 0;
            //Variable que conoce el resultado de la operación para saber cuantos shifts habrá que hacer (calculado en bucle) en multiplicación
            // y también para hacer la siguiente operación sabiendo que en el registro t2 en mips tenemos el resultado de la operación anterior
            int resultat = 0;

            switch(entries.get(i).getOp().getName()) {
                case "+":
                    //Miraremos cual es la variable a sumar para buscar su offset (para el fragment pointer)
                    //En el primer caso comprovamos si los dos operandos son variables
                    if (symbolTable.existsKey(entries.get(i).getOP1().getName(), -2) == -2
                            && symbolTable.existsKey(entries.get(i).getOP2().getName(), -2) == -2) {

                        //Obtenemos el offset del operando 1
                        offset = offsetRegisters.get(entries.get(i).getOP1().getName());
                        //Obtenemos el offset del operando 2
                        offset2 = offsetRegisters.get(entries.get(i).getOP2().getName());

                        //Primero cargamos los dos valores de los offset en la stack donde se encuentran las variables
                        //Así tendremos en registro t2 el valor de la variable operando 1 y en el registro t3 el del operando 2.
                        stringBuilder.append("LW   $t2, " + offset + "($fp)\n"
                                + "LW   $t3, " + offset2 + "($fp)\n");
                        //Seguidamente los sumamos
                        stringBuilder.append("ADDU   $t2, $t2, $t3");

                        if (!countSentences.isEmpty()) {
                            countSentences.set(countSentences.size() - 1, countSentences.get(countSentences.size() - 1) + 3);
                        }

                        //TODO Resultats habrá que meterlo en el registro de una entrada de la tabla de código intermedio porque es el acumulado
                        // y es mejor de manera conceptual
                        resultat = Integer.parseInt(symbolTable.getSymbol(entries.get(i).getOP1().getName(), -2).getValue())
                                + Integer.parseInt(symbolTable.getSymbol(entries.get(i).getOP2().getName(), -2).getValue());

                    } else if (symbolTable.existsKey(entries.get(i).getOP1().getName(), -2) == -2 && !valueAcumulated) {
                        //Si el operando 1 existe en la tabla de simbolos miramos su offset, es decir, en qué fragmento se encuentra
                        offset = offsetRegisters.get(entries.get(i).getOP1().getName());
                        value = Integer.parseInt(entries.get(i).getOP2().getName());
                        //Cargamos el valor de la variable operando 1 en el registro t2 para despues sumarlo con el valor del operando 2 que directamente es un número.
                        stringBuilder.append("LW   $t2, " + offset + "($fp)\n"
                                + "ADDIU   $t2, $t2, " + value + "\n");

                        if (!countSentences.isEmpty()) {
                            countSentences.set(countSentences.size() - 1, countSentences.get(countSentences.size() - 1) + 2);
                        }

                        resultat = Integer.parseInt(symbolTable.getSymbol(entries.get(i).getOP1().getName(), -2).getValue()
                                + Integer.parseInt(entries.get(i).getOP2().getName()));

                    } else if (symbolTable.existsKey(entries.get(i).getOP2().getName(), -2) == -2 && !valueAcumulated) {
                        //Si el operando 1 no existe, miramos si existe operando 2 al que tendremos que mirar su offset, es decir, en que fragmento se encuentra
                        if (offsetRegisters.containsKey(entries.get(i).getOP2().getName())) {
                            offset = offsetRegisters.get(entries.get(i).getOP2().getName());
                            value = Integer.parseInt(entries.get(i).getOP1().getName());
                        }
                        //Cargamos el valor de la variable operando 2 en el registro t2 para despues sumarlo con el valor del operando 1 que directamente es un número.
                        stringBuilder.append("LW   $t2, " + offset + "($fp)\n"
                                + "ADDIU   $t2, $t2, " + value + "\n");

                        if (!countSentences.isEmpty()) {
                            countSentences.set(countSentences.size() - 1, countSentences.get(countSentences.size() - 1) + 2);
                        }

                        resultat = Integer.parseInt(entries.get(i).getOP1().getName())
                                + Integer.parseInt(symbolTable.getSymbol(entries.get(i).getOP2().getName(), -2).getValue());

                    } else if (!valueAcumulated) {
                        //Si no hay variable en la operación, es porque son dos números que se quieren sumar, entonces resolvemos
                        int value1 = Integer.parseInt(entries.get(i).getOP1().getName());
                        int value2 = Integer.parseInt(entries.get(i).getOP2().getName());
                        //Añadimos en registro t2 el valor del operando 1 (sumando con 0, así es la manera de hacerlo).
                        stringBuilder.append("ADDIU   $t2, $zero, " + value1 + "\n");
                        //Lo mismo para el operando 2 en el registro t3.
                        stringBuilder.append("ADDIU   $t3, $zero, " + value2 + "\n");
                        //Ahora ya se pueden sumar los valores que estan en los registros t2 y t3, poniendo el resultado en el registro t2.
                        stringBuilder.append("ADDU   $t2, $t2, $t3\n");

                        if (!countSentences.isEmpty()) {
                            countSentences.set(countSentences.size() - 1, countSentences.get(countSentences.size() - 1) + 3);
                        }

                        resultat = Integer.parseInt(entries.get(i).getOP1().getName())
                                + Integer.parseInt(entries.get(i).getOP2().getName());

                    } else {
                        //En este caso el valor viene acumulado así que se encuentra en el registro temporal t2
                        if (symbolTable.existsKey(entries.get(i).getOP2().getName(), -2) == -2) {
                            //Si existe variable operando 2
                            stringBuilder.append("ADDIU   $t2, $t2, "
                                    + symbolTable.getSymbol(entries.get(i).getOP2().getName(), -2).getValue() + "\n");

                            if (!countSentences.isEmpty()) {
                                countSentences.set(countSentences.size() - 1, countSentences.get(countSentences.size() - 1) + 1);
                            }

                            resultat = resultat
                                    + Integer.parseInt(symbolTable.getSymbol(entries.get(i).getOP2().getName(), -2).getValue());
                        } else {
                            //Si es un número directamente
                            stringBuilder.append("ADDIU   $t2, $t2, " + entries.get(i).getOP2().getName() + "\n");

                            if (!countSentences.isEmpty()) {
                                countSentences.set(countSentences.size() - 1, countSentences.get(countSentences.size() - 1) + 1);
                            }

                            resultat = resultat
                                    + Integer.parseInt(entries.get(i).getOP2().getName());
                        }
                    }

                    lastResult = resultat;
                    valueAcumulated = true;
                    break;

                case "-":
                    //Miramos cual es la variable a restar para buscar su offset (para el fragment pointer)
                    //En el primer caso comprovamos si los dos operandos son variables
                    if (symbolTable.existsKey(entries.get(i).getOP1().getName(), -2) == -2
                            && symbolTable.existsKey(entries.get(i).getOP2().getName(), -2) == -2
                            && !valueAcumulated) {

                        offset = offsetRegisters.get(entries.get(i).getOP1().getName());
                        offset2 = offsetRegisters.get(entries.get(i).getOP2().getName());
                        //Primero cargamos los dos valores del offset en memoria donde se encuentren
                        stringBuilder.append("LW   $t2, " + offset + "($fp)\n"
                                + "LW   $t3, " + offset2 + "($fp)\n");
                        //Seguidamente los restamos
                        stringBuilder.append("SUBU   $t2, $t3, $t2");

                        if (!countSentences.isEmpty()) {
                            countSentences.set(countSentences.size() - 1, countSentences.get(countSentences.size() - 1) + 3);
                        }

                        resultat = Integer.parseInt(symbolTable.getSymbol(entries.get(i).getOP2().getName(), -2).getValue())
                                - Integer.parseInt(symbolTable.getSymbol(entries.get(i).getOP1().getName(), -2).getValue());

                    } else if (symbolTable.existsKey(entries.get(i).getOP1().getName(), -2) == -2
                            && !valueAcumulated) {
                        //Si el operando 1 existe en la tabla de simbolos miramos su offset
                        offset = offsetRegisters.get(entries.get(i).getOP1().getName());
                        value = Integer.parseInt(entries.get(i).getOP2().getName());
                        stringBuilder.append("ADDIU   $t3, $zero, " + value + "\n"
                                + "LW   $t2, " + offset + "($fp)\n"
                                + "SUBU $t2, $t3, $t2\n");

                        if (!countSentences.isEmpty()) {
                            countSentences.set(countSentences.size() - 1, countSentences.get(countSentences.size() - 1) + 3);
                        }

                        System.out.println("Estamos en la linea " + entries.get(i).getOp().getName());
                        resultat = Integer.parseInt(entries.get(i).getOP2().getName())
                                - Integer.parseInt(symbolTable.getSymbol(entries.get(i).getOP1().getName(), -2).getValue());

                    } else if (symbolTable.existsKey(entries.get(i).getOP2().getName(), -2) == -2
                            && !valueAcumulated) {
                        //Si el operando 1 no existe, miramos si existe operando 2 al que tendremos que mirar su offset (si se ha aplicado optimización)
                        if (offsetRegisters.containsKey(entries.get(i).getOP2().getName())) {
                            offset = offsetRegisters.get(entries.get(i).getOP2().getName());
                            value = Integer.parseInt(entries.get(i).getOP1().getName());

                            stringBuilder.append("LW $t2, " + offset + "\n"
                                    + "ADDIU $t2, $t2, -" + value + "\n");

                            if (!countSentences.isEmpty()) {
                                countSentences.set(countSentences.size() - 1, countSentences.get(countSentences.size() - 1) + 2);
                            }
                        }

                        resultat = Integer.parseInt(symbolTable.getSymbol(entries.get(i).getOP2().getName(), -2).getValue())
                                - Integer.parseInt(entries.get(i).getOP1().getName());
                    } else if (!valueAcumulated) {
                        //Si no hay variable en la operación, es porque son dos numeros que se quieren restar, entonces resolvemos
                        int value1 = Integer.parseInt(entries.get(i).getOP1().getName());
                        int value2 = Integer.parseInt(entries.get(i).getOP2().getName());
                        stringBuilder.append("ADDIU   $t2, $zero, " + value1 + "\n");
                        stringBuilder.append("ADDIU   $t3, $zero, " + value2 + "\n");
                        stringBuilder.append("SUBU   $t2, $t3, $t2\n");

                        if (!countSentences.isEmpty()) {
                            countSentences.set(countSentences.size() - 1, countSentences.get(countSentences.size() - 1) + 3);
                        }

                        resultat = Integer.parseInt(entries.get(i).getOP2().getName())
                                - Integer.parseInt(entries.get(i).getOP1().getName());
                    } else {
                        //En este caso el valor viene acumulado así que se encuentra en el registro temporal t2
                        if (symbolTable.existsKey(entries.get(i).getOP2().getName(), -2) == -2) {
                            stringBuilder.append("ADDIU   $t3, $zero, "
                                    + symbolTable.getSymbol(entries.get(i).getOP2().getName(), -2).getValue() + "\n");
                            if (!countSentences.isEmpty()) {
                                countSentences.set(countSentences.size() - 1, countSentences.get(countSentences.size() - 1) + 1);
                            }

                            resultat = Integer.parseInt(symbolTable.getSymbol(entries.get(i).getOP2().getName(), -2).getValue())
                                    - resultat;
                        } else {
                            stringBuilder.append("ADDIU   $t3, $zero, "
                                    + entries.get(i).getOP2().getName() + "\n");

                            if (!countSentences.isEmpty()) {
                                countSentences.set(countSentences.size() - 1, countSentences.get(countSentences.size() - 1) + 1);
                            }

                            resultat = Integer.parseInt(entries.get(i).getOP2().getName())
                                    - resultat;
                        }
                        stringBuilder.append("SUBU   $t2, $t3, $t2\n");
                        if (!countSentences.isEmpty()) {
                            countSentences.set(countSentences.size() - 1, countSentences.get(countSentences.size() - 1) + 1);
                        }
                    }

                    lastResult = resultat;
                    valueAcumulated = true;
                    break;

                case "*":
                    //El caso de la multiplicación es diferente a las otras operaciones.
                    //El método usado es poner zeros por el lado de menos peso para multiplicar por dos cuantos se puedan.
                    //Cuando ya no hay que poner más zeros, tenemos un valor resultante que es el que se suma para dar el resultado correcto.
                    //Es decir 3x3 = 9. Entonces haremos un shift al valor 3 (poner un 0 lsb) y el valor será 6,
                    // que ya no puede ser multiplicado por dos porqué pasaria a ser 12, entonces se sumaria 3.
                    //Entonces serian un shift y una suma con 3.

                    //Variable para saber cual es el numero al que hacemos shift left logical (poner zeros lsb)
                    int num = 0;
                    //Variable para contar los shifts que habrá que hacerle al num.
                    int shifts = -1;

                    //Variable para saber si es el caso que la operación es entre dos números (ninguna variable)
                    boolean noVarsInOperation = false;

                    //En el primer caso comprovamos si los dos operandos son variables
                    if (symbolTable.existsKey(entries.get(i).getOP1().getName(), -2) == -2
                            && symbolTable.existsKey(entries.get(i).getOP2().getName(), -2) == -2) {

                        offset = offsetRegisters.get(entries.get(i).getOP1().getName());
                        num = Integer.parseInt(symbolTable.getSymbol(entries.get(i).getOP1().getName(), -2).getValue());
                        resultat = num * Integer.parseInt(symbolTable.getSymbol(entries.get(i).getOP2().getName(), -2).getValue());



                    } else if (symbolTable.existsKey(entries.get(i).getOP1().getName(), -2) == -2
                            && !valueAcumulated) {
                        //Si el operando 1 existe en la tabla de simbolos miramos su offset
                        offset = offsetRegisters.get(entries.get(i).getOP1().getName());
                        num = Integer.parseInt(symbolTable.getSymbol(entries.get(i).getOP1().getName(), -2).getValue());
                        resultat = num * Integer.parseInt(entries.get(i).getOP2().getName());


                    } else if (symbolTable.existsKey(entries.get(i).getOP2().getName(), -2) == -2
                            && !valueAcumulated) {
                        //Si el operando 1 no existe, será operando 2 al que tendremos que mirar su offset (si se ha aplicado optimización)
                        System.out.println("Mips code is:\n" + stringBuilder.toString());
                        offset = offsetRegisters.get(entries.get(i).getOP2().getName());
                        num = Integer.parseInt(symbolTable.getSymbol(entries.get(i).getOP2().getName(), -2).getValue());
                        resultat = num * Integer.parseInt(entries.get(i).getOP1().getName());

                        //Es posible que tengamos el operando 1 en formato registro (acumulado anteriormente).
                        //Entonces actuamos de la siguiente manera sabiendo que el valor está en el registro t2
                    } else if (!valueAcumulated) {
                        //En este caso entraremos cuando la operación sea entre dos numeros (ninguna variable)
                        noVarsInOperation = true;

                        num = Integer.parseInt(entries.get(i).getOP1().getName());
                        resultat = num * Integer.parseInt(entries.get(i).getOP2().getName());
                    } else {
                        num = lastResult;

                        if (symbolTable.existsKey(entries.get(i).getOP2().getName(), -2) == -2) {
                            resultat = num * Integer.parseInt(symbolTable.getSymbol(entries.get(i).getOP2().getName(), -2).getValue());
                        } else {
                            resultat = num * Integer.parseInt(entries.get(i).getOP2().getName());
                        }
                    }

                    //TODO Habrá que mirar si el valor es negativo para todas las operaciones
                    /*if (resultat < 0) {
                        resultat = Math.abs(resultat);
                    }*/

                    int aux = num;
                    //Lo que queremos conseguir con el bucle es saber cuantos shifts (añadir zeros LSB (multiplicación por dos))
                    // hay que hacer y luego sumar restante para conseguir el resultado que queremos
                    while(aux <= resultat) {
                        aux = aux * 2;
                        shifts++;
                    }

                    if (noVarsInOperation) {
                        stringBuilder.append("ADDIU   $t2, $zero, " + entries.get(i).getOP1().getName() + "\n");
                    } else if (!valueAcumulated) {
                        stringBuilder.append("LW   $t2, " + offset + "($fp)\n");
                    }
                    if (!countSentences.isEmpty()) {
                        countSentences.set(countSentences.size() - 1, countSentences.get(countSentences.size() - 1) + 1);
                    }
                    //Si el resultado es el numero será porque se ha multiplicado por uno,
                    // entonces no hace falta hacer ni shift ni addiu, directamente cargar el valor de la variable en el registro
                    if (resultat != num) {
                        int restant = resultat - (aux / 2);
                        stringBuilder.append("SLL   $t2, $t2, " + shifts + "\n");
                        if (restant != 0) {
                            stringBuilder.append("ADDIU   $t2, $t2, " + restant + "\n");
                            if (!countSentences.isEmpty()) {
                                countSentences.set(countSentences.size() - 1, countSentences.get(countSentences.size() - 1) + 1);
                            }
                        }
                    }
                    if (!countSentences.isEmpty()) {
                        countSentences.set(countSentences.size() - 1, countSentences.get(countSentences.size() - 1) + 1);
                    }

                    lastResult = resultat;
                    valueAcumulated = true;
                    break;

                case "=":
                    //Comprovamos si venimos de acumulación de registro para hacer un STOREWORD directamente
                    if (symbolTable.existsKey(entries.get(i).getOP1().getName(), -2) == -2){
                        //En este caso entraremos si se quiere asignar un valor de una variable.
                        //Cogeremos el valor de la variable y lo añadiremos a la dirección de la nueva variable también.
                        int varOffset = offsetRegisters.get(entries.get(i).getOP1().getName());
                        stringBuilder.append("LW   $t2, " + varOffset + "($fp)\n");
                        if (!countSentences.isEmpty()) {
                            countSentences.set(countSentences.size() - 1, countSentences.get(countSentences.size() - 1) + 1);
                        }
                    } else if (!valueAcumulated) {
                        //En este caso entraremos si no venimos de acumulación ni tampoco se quiere asignar valor que lo contiene una variable.
                        //Entonces directamente cargaremos el valor en el registro y lo guardaremos a la dirección que toque.
                        stringBuilder.append("ADDIU   $t2, $zero, " + entries.get(i).getOP1().getName() + "\n");
                        if (!countSentences.isEmpty()) {
                            countSentences.set(countSentences.size() - 1, countSentences.get(countSentences.size() - 1) + 1);
                        }
                    } else {
                        //Si venimos de acumulación, el valor a guardar será el valor que contiene el registro
                        //Entonces no cargamos nada porque ya está en el registro.
                    }

                    if (offsetRegisters.containsKey(entries.get(i).getOP2().getName())) {

                        stringBuilder.append("SW   $t2, " + offsetRegisters.get(entries.get(i).getOP2().getName()) + "($fp)\n");

                    } else {
                        //Ponemos en el hashmap el valor del offset desde el fragment pointer donde se encontrará una variable en memoria.
                        offsetRegisters.put(entries.get(i).getOP2().getName(), fp);
                        stringBuilder.append("SW   $t2, " + fp + "($fp)\n");
                        //Obtenemos el número de bytes que necesita para ser guardado el tipo de dato (char = 1 byte; intg = 4 bytes...)
                        int bytes = numBytes(entries.get(i).getOP2());
                        fp = fp - bytes;
                    }

                    if (!countSentences.isEmpty()) {
                        countSentences.set(countSentences.size() - 1, countSentences.get(countSentences.size() - 1) + 1);
                    }

                    //Como se ha hecho asignación, añadimos el valor en la tabla de símbolos.
                    symbolTable.getSymbol(entries.get(i).getOP2().getName(), -2).setValue(entries.get(i).getOP1().getName());
                    valueAcumulated = false;
                    break;

                case "lt":

                    //En el primer caso comprovamos si los dos operandos son variables
                    if (symbolTable.existsKey(entries.get(i).getOP1().getName(), -2) == -2
                            && symbolTable.existsKey(entries.get(i).getOP2().getName(), -2) == -2) {

                        //Obtenemos los offset en mips de las variables a comparar
                        offset = offsetRegisters.get(entries.get(i).getOP1().getName());
                        offset2 = offsetRegisters.get(entries.get(i).getOP2().getName());
                        //Primero cargamos los dos valores del offset en memoria donde se encuentren
                        stringBuilder.append("LW    $t2, " + offset + "($fp)\n"
                                + "LW   $t3, " + offset2 + "($fp)\n");
                        //Seguidamente los restamos
                        stringBuilder.append("SLT   $t2, $t2, $t3\n");

                        if (!countSentences.isEmpty()) {
                            countSentences.set(countSentences.size() - 1, countSentences.get(countSentences.size() - 1) + 3);
                        }

                        doBEQ = true;


                    } else if (symbolTable.existsKey(entries.get(i).getOP1().getName(), -2) == -2) {
                        //Si el operando 1 existe en la tabla de simbolos miramos su offset
                        offset = offsetRegisters.get(entries.get(i).getOP1().getName());
                        value = Integer.parseInt(entries.get(i).getOP2().getName());
                        stringBuilder.append("LW    $t2, " + offset + "($fp)\n"
                                + "SLT    $t2, $t2, " + value + "\n");

                        if (!countSentences.isEmpty()) {
                            countSentences.set(countSentences.size() - 1, countSentences.get(countSentences.size() - 1) + 2);
                        }

                        doBEQ = true;

                    } else if (symbolTable.existsKey(entries.get(i).getOP2().getName(), -2) == -2) {
                        //Si el operando 1 no existe, será operando 2 al que tendremos que mirar su offset (si se ha aplicado optimización)
                        offset = offsetRegisters.get(entries.get(i).getOP2().getName());
                        value = Integer.parseInt(entries.get(i).getOP1().getName());

                        stringBuilder.append("LW    $2, " + offset + "($fp)\n"
                                + "SLT  $t2, $t2, " + (value + 1) + "\n");

                        if (!countSentences.isEmpty()) {
                            countSentences.set(countSentences.size() - 1, countSentences.get(countSentences.size() - 1) + 2);
                        }

                        doBEQ = false;

                        //Es posible que tengamos el operando 1 en formato registro (acumulado anteriormente).
                        //Entonces actuamos de la siguiente manera sabiendo que el valor está en el registro t2
                    } else {
                        //En este caso entraremos cuando la operación sea entre dos numeros (ninguna variable)
                        int value1 = Integer.parseInt(entries.get(i).getOP1().getName());
                        int value2 = Integer.parseInt(entries.get(i).getOP2().getName());

                        stringBuilder.append("ADDIU   $t2, $zero, " + value1 + "\n");
                        stringBuilder.append("SLT   $t2, $t2, " + value2 + "\n");

                        if (!countSentences.isEmpty()) {
                            countSentences.set(countSentences.size() - 1, countSentences.get(countSentences.size() - 1) + 2);
                        }

                        doBEQ = true;
                    }

                    break;

                case "if":

                    countSentences.add(0);

                    String tag = "L" + timestamp++;
                    tags.push(tag);
                    if (doBEQ) {
                        stringBuilder.append("BEQ   $t2, $zero, " + tag + "\n");
                    } else {
                        stringBuilder.append("BNE   $t2, $zero, " + tag + "\n");
                    }
                    break;

                case "endif":

                    countEnds++;

                    stringBuilder.append(tags.pop() + ":\n");

                    if (countEnds == countSentences.size()) {
                        countEnds = 0;
                        countSentences = new ArrayList<>();
                    }
                    break;

                case "initwhile":
                    countSentences.add(0);

                    String sentencesInWhileBlockTag = "L" + timestamp++;
                    tags.push(sentencesInWhileBlockTag);

                    System.out.println("Ultimes linies son: ");
                    /*String[] lines = stringBuilder.toString().split("\\n");
                    stringBuilder = new StringBuilder();
                    for (int l = 0; l < lines.length - 2; l++) {
                        stringBuilder.append(lines[l] + "\n");
                    }*/
                    System.out.println("Fi ultimes linies");


                    String comparationInWhileTag = "L" + timestamp++;
                    tags.push(comparationInWhileTag);

                    //stringBuilder.append(comparationInWhileTag);
                    //stringBuilder.append(tag2 + ":\n");
                    //stringBuilder.append("     " + lines[lines.length - 2] + "\n");
                    //stringBuilder.append("     " + lines[lines.length - 1] + "\n");


                    /*String whileComparation = comparationInWhileTag + ":\n"
                            + "     " + lines[lines.length - 2] + "\n"
                            + "     " + lines[lines.length - 1] + "\n";*/


                    break;

                case "endwhile":
                    System.out.println(stringBuilder.toString());
                    countEnds++;

                    int countLines = 0;
                    for (int w = 0; w < countSentences.size(); w++) {
                        System.out.println(countSentences.get(w));
                        countLines = countLines + countSentences.get(w);
                    }

                    String[] lines = stringBuilder.toString().split("\\n");
                    stringBuilder = new StringBuilder();
                    for (int l = 0; l < lines.length - (countLines + 2); l++) {
                        stringBuilder.append(lines[l] + "\n");
                    }

                    System.out.println("New stringbuilder is \n" + stringBuilder.toString());

                    System.out.println("lines.length is " + lines.length);
                    System.out.println("countLines is " + countLines);
                    System.out.println("Lines.length - countLines is " + (lines.length - countLines));

                    for (int w = 0; w < countSentences.size(); w++) {
                        String etiqueta = (String) tags.pop();
                        stringBuilder.append("B   " + etiqueta + ":\n");

                        String etiqueta2 = (String) tags.pop();
                        stringBuilder.append(etiqueta2 + ":\n");

                        for (int s = 0; s < countSentences.get(w); s++) {
                            stringBuilder.append(lines[(lines.length - (countLines)) + s * ( w + 1)] + "\n");
                        }

                        stringBuilder.append(etiqueta + ":\n");
                        for (int s = 0; s < countSentences.get(w); s++) {
                            stringBuilder.append(lines[(lines.length - (countLines + 2)) + s * ( w + 1)] + "\n");
                        }

                        if (doBEQ) {
                            stringBuilder.append("BEQ   $t2, $zero, " + etiqueta2 + "\n");
                        } else {
                            stringBuilder.append("BNE   $t2, $zero, " + etiqueta2 + "\n");
                        }

                        System.out.println("Por el momento llevamos: \n "+stringBuilder.toString());
                    }

                    /*stringBuilder.append(tags.pop() + ":\n");
                    stringBuilder.append(whileSentences);

                    stringBuilder.append(tags.pop() + ":\n");
                    stringBuilder.append(whileComparation);*/

                    if (countEnds == countSentences.size()) {
                        countEnds = 0;
                        countSentences = new ArrayList<>();
                    }
                    break;


            }
        }

        System.out.println("Mips code is:\n" + stringBuilder.toString());
        this.outputCode = stringBuilder.toString();
    }

    /**
     * Función que devuelve el número de bytes que necesita un token ya que conocemos su tipo.
     * Sabiendo el número de bytes sabremos donde guardar correctamente en la stack al generar código mips.
     * @param token Token
     * @return int: Número de bytes
     */
    private int numBytes(Token token) {

        switch (symbolTable.getSymbol(token.getName(), -2).getType()) {
            case ConstValues.INTG_VALUE:
                return 4;
            case ConstValues.CHAR_VALUE:
                return 1;
            default:
                return 0;
        }
    }

    public static void dumpCodeIntoFile(String filename, String mipsCode){
        File file = new File(filename);
        try {
            FileWriter fw = new FileWriter(file);
            fw.write(mipsCode, 0, mipsCode.length());
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
