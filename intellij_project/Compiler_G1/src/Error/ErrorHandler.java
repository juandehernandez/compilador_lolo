package Error;

import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.SynchronousQueue;

public class ErrorHandler {
    private ArrayList<Error> errorQueue;

    public ErrorHandler() {
        this.errorQueue = new ArrayList<Error>();
    }

    public void addError(Error e){
        errorQueue.add(e);
    }

    public void dumpAllErrors(){
        for(Error e : errorQueue){
            System.err.println(e.getMessage());
        }
    }

    public int getErrorCount(){
        return errorQueue.size();
    }

    public ArrayList<Error> getErrorQueue() {
        return errorQueue;
    }
}
