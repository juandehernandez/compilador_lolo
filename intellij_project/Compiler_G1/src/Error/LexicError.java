package Error;

public class LexicError extends Error {

    public static int ERROR_LEXIC_ILLEGAL_IDENTIFIER = 0;
    public static int ERROR_LEXIC_NUMBER_FORMAT = 1;
    public static int ERROR_LEXIC_UNKNOWN = 2;
    public static int ERROR_LEXIC_EOF = 3;

    private static String[] MSG_ERROR_LEXIC = {
            "Ilegal identifier name",
            "Incorrect number format at ",
            "Unknown identifier ",
            "Unexpected EOF character"
    };

    //--------------------------------------------------------------------------//

    private int lexicErrorType;

    public LexicError(int lineLocation, String fileLocation, int lexicErrorType) throws ErrorTypeException {
        super(lineLocation, fileLocation);
        if(lexicErrorType < 0 || lexicErrorType > 3){
            throw new ErrorTypeException(lexicErrorType, this);
        }
        this.lexicErrorType = lexicErrorType;
    }

    @Override
    public String getMessage() {
        return String.format(super.ERROR_MESSAGE_FORMAT, fileLocation, lineLocation, MSG_ERROR_LEXIC[lexicErrorType]);
    }

    @Override
    public String getName() {
        return "LexicError";
    }
}
