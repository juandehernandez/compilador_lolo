package Error;

public abstract class Error {

    /**
     * Ejemplo:
     * fichero.lolo:213: Ilegal identifier name "<token>"
     */
    protected final String ERROR_MESSAGE_FORMAT = "%s:%d: %s.";

    protected int lineLocation;
    protected String fileLocation;

    public Error(int lineLocation, String fileLocation) {
        this.lineLocation = lineLocation;
        this.fileLocation = fileLocation;
    }

    public int getLineLocation() {
        return lineLocation;
    }

    public String getFileLocation() {
        return fileLocation;
    }

    public abstract String getMessage();

    public abstract String getName();

    public class ErrorTypeException extends Exception{
        private int errorType;
        private Error origin;

        public ErrorTypeException(int errorType, Error origin){
            this.errorType = errorType;
            this.origin = origin;
        }

        public String getMessage(){
            return String.format("Unknown error type %d for %s.", errorType, origin.getName());
        }
    }
}
