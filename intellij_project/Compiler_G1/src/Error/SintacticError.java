package Error;

public class SintacticError extends Error {

    public static int ERROR_SINTACTIC_UNDECLARED_IDENTIFIER = 0;
    public static int ERROR_SINTACTIC_CLOSE_CONTEXT = 1;
    public static int ERROR_SINTACTIC_DELIMITER = 2;
    public static int ERROR_SINTACTIC_UNEXPECTED_FIRST = 3;
    public static int ERROR_SINTACTIC_ILLEGAL_IDENTIFIER_FOLLOW = 4;
    public static int ERROR_SINTACTIC_NOT_FOLLOWING_SYMBOL = 5;
    public static int ERROR_SINTACTIC_NOT_END_IDENTIFIER = 6;

    private static String[] MSG_ERROR_SINTACTIC = {
            "Undeclared identifier",
            "Expected ')'",
            "Expected ';'",
            "Unexpected first identifier",
            "Unexpected identifier",
            "Expected a following identifier",
            "Expected end identifier"
    };

    //--------------------------------------------------------------------------//

    private int sintacticErrorType;

    public SintacticError(int lineLocation, String fileLocation, int sintacticErrorType) throws ErrorTypeException {
        super(lineLocation, fileLocation);
        if(sintacticErrorType < 0 || sintacticErrorType > MSG_ERROR_SINTACTIC.length - 1){
            throw new ErrorTypeException(sintacticErrorType, this);
        }
        this.sintacticErrorType = sintacticErrorType;
    }

    @Override
    public String getMessage() {
        return String.format(super.ERROR_MESSAGE_FORMAT, fileLocation, lineLocation, MSG_ERROR_SINTACTIC[sintacticErrorType]);
    }

    @Override
    public String getName() {
        return "SintacticError";
    }
}
