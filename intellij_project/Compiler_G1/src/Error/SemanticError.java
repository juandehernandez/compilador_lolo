package Error;

public class SemanticError extends Error {

    public static int ERROR_SEMANTIC_ALREADY_DECLARED_IDENTIFIER = 0;
    public static int ERROR_SEMANTIC_UNDECLARED_IDENTIFIER = 1;
    public static int ERROR_SEMANTIC_UNEXPECTED_IDENTIFIER_TYPE = 2;


    private static String[] MSG_ERROR_SEMANTIC = {
            "Identifier already declared",
            "Undeclared identifier",
            "Unexpected identifier type",
    };

    //--------------------------------------------------------------------------//

    private int semanticErrorType;

    public SemanticError(int lineLocation, String fileLocation, int semanticErrorType) throws ErrorTypeException {
        super(lineLocation, fileLocation);
        if(semanticErrorType < 0 || semanticErrorType > MSG_ERROR_SEMANTIC.length - 1){
            throw new ErrorTypeException(semanticErrorType, this);
        }
        this.semanticErrorType = semanticErrorType;
    }

    @Override
    public String getMessage() {
        return String.format(super.ERROR_MESSAGE_FORMAT, fileLocation, lineLocation, MSG_ERROR_SEMANTIC[semanticErrorType]);
    }

    @Override
    public String getName() {
        return "SemanticError";
    }
}
