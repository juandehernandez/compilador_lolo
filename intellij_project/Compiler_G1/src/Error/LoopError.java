package Error;

public class LoopError extends Error{

    private static String MSG_ERROR_LOOP = "Loop never ends";

    public LoopError(int lineLocation, String fileLocation){
        super(lineLocation, fileLocation);
    }

    @Override
    public String getMessage() {
        return String.format(super.ERROR_MESSAGE_FORMAT, fileLocation, lineLocation, MSG_ERROR_LOOP);
    }

    @Override
    public String getName() {
        return "Loop Error";
    }
}
