import CodeOptimization.CodeOptimizer;
import Error.Error;
import Error.ErrorHandler;
import Mips.MipsGenerator;
import Symbols.SymbolTable;
import Syntactic.Parser;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Lolo language compiler.
 */
public class LLC {

    private String sourceFile;
    private String outputFile;

    public LLC(String sourceFile, String outputFile){
        this.sourceFile = sourceFile;
        this.outputFile = outputFile;
    }

    public void compile(){

        SymbolTable symbolTable = new SymbolTable();
        // Initialize queues

        try {
            //parser = new Parser(scannerOutParserInQueue);
            ErrorHandler errorHandler = new ErrorHandler();
            Parser parser = new Parser(symbolTable, sourceFile, errorHandler);
            boolean correct = parser.parse();
            /*if (correct){
                // Montar parse tree
                parser.createParseTree();
                // Semantico -> TODO inside of the create parse tree

            }*/
            //boolean correct = parser.run();
            if (correct){
                // Everything in code is correct
                CodeOptimizer codeOptimizer = new CodeOptimizer(parser.getParseTree(), symbolTable);
                MipsGenerator assemblyGenerator = codeOptimizer.getMipsGenerator();
                codeOptimizer.optimize();
                //assemblyGenerator.generateMipsCode();
                int i = 0;
                int size = codeOptimizer.getIntermediateTable().size();
                String mips = "";
                System.out.println("Size of intermid: " + size);
                while (i < size){
                    //assemblyGenerator.dumpCodeIntoFile(outputFile);
                    i = assemblyGenerator.recursiveMips(codeOptimizer.getIntermediateTable().getEntries(), i);
                }
                mips = assemblyGenerator.mips;
                System.out.println("MIPS CODE:");
                System.out.println(mips);

                MipsGenerator.dumpCodeIntoFile(outputFile, mips);

            } else {
                errorHandler.dumpAllErrors();
            }

        } catch (FileNotFoundException e) {
            System.out.println("No such file or directory. Impossible to compile.");
            e.printStackTrace();
        }
        /*catch (Error.ErrorTypeException | IOException e) {
            e.printStackTrace();
        }*/


    }

}
