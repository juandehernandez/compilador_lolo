package Syntactic;

import Lexicon.Token;

import java.util.ArrayList;

public class SentenceBlock {
    // Const values
    public static final int BLOCK_INCORRECT = -1;
    public static final int BLOCK_ROOT = 0;
    public static final int BLOCK_DECLARACION = 1;
    public static final int BLOCK_ASIGNACION = 2;
    public static final int BLOCK_IF = 3;
    public static final int BLOCK_WHILE = 4;
    public static final int BLOCK_SUBROOT = -2;


    // Attributes
    private int type;
    private ArrayList<Token> root; // cabeceras if y while
    private ArrayList<SentenceBlock> blocks;

    // Constructor
    public SentenceBlock(int type){
        this.type = type;
        this.root = new ArrayList<Token>();
        blocks = new ArrayList<SentenceBlock>();
    }
    public SentenceBlock(int type, ArrayList<Token> root){
        this.type = type;
        this.root = root;
        blocks = new ArrayList<SentenceBlock>();
    }

    // Getters & Setters
    public int getType() {
        return type;
    }
    public void setType(int type) {
        this.type = type;
    }
    public ArrayList<Token> getRoot() {
        return root;
    }
    public void setRoot(ArrayList<Token> root) {
        this.root = root;
    }
    public ArrayList<SentenceBlock> getBlocks() {
        return blocks;
    }
    public void setBlocks(ArrayList<SentenceBlock> blocks) {
        this.blocks = blocks;
    }
}
