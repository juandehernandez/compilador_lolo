package Syntactic;

import IntermediateCode.IntermediateTable;
import IntermediateCode.IntermediateTableEntry;
import Lexicon.ConstValues;
import Lexicon.Token;
import Symbols.SymbolTable;
import Semantic.Semantic;
//import com.sun.istack.internal.Nullable;
import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Arbol sintactico.
 */
public class ParseTree {

    //Raíz del arbol de la totalidad del programa escrito
    public ParseTreeNode root;
    private ArrayList<Token> sentencia;
    private Semantic semantic;

    public ParseTree(){
        this.root = new ParseTreeNode();
        //this.root.element = root;
        this.root.children = new ArrayList<>();
    }

    public ParseTreeNode getRoot(){return root;}

    /**
     * Genera el codigo intermedio del programa de cara a las optimizaciones de
     * codigo.
     * @param symbolTable Tabla de simbolos del programa.
     * @return the table that contains the intermediate code
     */
    public IntermediateTable generateIntermediateCode(SymbolTable symbolTable){
        IntermediateTable intermediateCode = new IntermediateTable(symbolTable);
        //ParseTreeNode whileTreeRoot = createWhileParseTreeAUX();
        //buildIntermediateTable(intermediateCode, whileTreeRoot);
        for(ParseTreeNode sentence : root.children){

            buildIntermediateTable(intermediateCode, sentence);
        }

        mostrarTablaDeCodigoIntermedio(intermediateCode);
        return intermediateCode;
    }

    private void mostrarTablaDeCodigoIntermedio(IntermediateTable intermediateCode) {
        int numEntrie = intermediateCode.size();

        for (int i = 0; i < numEntrie; i++){
            IntermediateTableEntry entry = intermediateCode.getEntries().get(i);
            System.out.println(entry.getOP1().getName() + " | " + entry.getOp().getName() + " | " + entry.getOP2().getName() + " | " + entry.getR().getName());
        }
    }


    //Monta recursivamente la tabla de codigo intermedio
    private Token buildIntermediateTable(IntermediateTable table, ParseTreeNode node){
        // De momento lo hacemos solo para el simple código que tenemos (operacion y asignacion)
        Token OP1;
        Token OP2;

        //System.out.println("    NODO MIRANDO --> " + node.getElement().getName());

        if (node.getElement().getType_id() == ConstValues.IF){

            int initial = table.size();
            buildIntermediateTable(table, node.children.get(0));

            int indexFromJump = table.size();
            //Añadimos una linea de operador if a la tabla de código intermedio
            IntermediateTableEntry ifEntry = new IntermediateTableEntry(table.getEntries().get(indexFromJump - 1).getR(),
                    new Token(ConstValues.IF, Integer.toString(initial)), new Token(ConstValues.IF, "if"), new Token(ConstValues.IF, "if-R"));
            table.getEntries().add(ifEntry);

            ParseTreeNode subroot = node.children.get(1);
            int subrootLen = subroot.children.size();
            for (int i = 0; i < subrootLen; i++){
                buildIntermediateTable(table, subroot.children.get(i));
            }

            int finalLine = table.size();
            //Añadimos una linea de operador endif a la tabla de código intermedio que será útil para identificar en mips
            // cuando llega el final de un if para poner la etiqueta (TAG) de la Stack (LIFO)
            IntermediateTableEntry ifEntryEnd = new IntermediateTableEntry(table.getEntries().get(finalLine - 1).getR(),
                    new Token(ConstValues.IF, "null"), new Token(ConstValues.IF, "endif"), new Token(ConstValues.IF, "if-R"));
            //table.getEntries().add(ifEntryEnd);

            //Actualizamos el campo R del if que contiene las sentencias con el número de línea
            // que tendrá que saltar si no se cumple la condición del if statement
            table.getEntries().get(indexFromJump).setR(new Token(ConstValues.IF, String.valueOf(finalLine)));

            return null;

        }else if (node.getElement().getType_id() == ConstValues.WHILE){

            int indexPrimeraLineaComparacion = table.size();

            buildIntermediateTable(table, node.children.get(0));

            int indexFromJump = table.size();

            IntermediateTableEntry ifEntry = new IntermediateTableEntry(table.getEntries().get(indexFromJump - 1).getR(),
                    new Token(ConstValues.IF, Integer.toString(indexPrimeraLineaComparacion)), new Token(ConstValues.WHILE, "initwhile"), new Token(ConstValues.IF, "if"));
            table.getEntries().add(ifEntry);

            ParseTreeNode subroot = node.children.get(1);
            int subrootLen = subroot.children.size();
            for (int i = 0; i < subrootLen; i++){
                buildIntermediateTable(table, subroot.children.get(i));
            }

            IntermediateTableEntry ifEntry2 = new IntermediateTableEntry(new Token(ConstValues.WHILE, "true"),
                    new Token(ConstValues.WHILE, "null"), new Token(ConstValues.WHILE, "endwhile"), new Token(ConstValues.IF, String.valueOf(indexPrimeraLineaComparacion)));
            table.getEntries().add(ifEntry2);

            int finalLine = table.size();
            table.getEntries().get(indexFromJump).setR(new Token(ConstValues.IF, String.valueOf(finalLine)));

            return null;

        }else{
            if (node.children.get(1).getChildren() == null) {
                OP1 = node.children.get(1).element;
            } else {
                OP1 = buildIntermediateTable(table, node.children.get(1));
            }

            if (node.children.get(0).children != null){
                OP2 = buildIntermediateTable(table, node.children.get(0));
            }else{
                OP2 = node.children.get(0).element;
            }

            return table.addEntry(OP1, OP2, node.element);
        }
    }
    //----------------------------------------------------------------------------------------------------------------//

    public static class ParseTreeNode{
        Token element;
        boolean isSentence;
        int sentenceType;
        ArrayList<ParseTreeNode> children;


        public Token getElement() { return element; }
        public ArrayList<ParseTreeNode> getChildren() { return children; }
        public void setElement(Token element) { this.element = element; }
        public void setSentence(boolean sentence) { isSentence = sentence; }
        public void setSentenceType(int sentenceType) { this.sentenceType = sentenceType; }
        public void setChildren(ArrayList<ParseTreeNode> children) { this.children = children; }
        public boolean isSentence() { return isSentence; }
        public int getSentenceType() { return sentenceType; }
    }


    //**********************************************************

    public static ParseTreeNode mountAsignationDeclaration(ArrayList<Token> sentence) {
        int sentenceType = 0;
        //En caso de declaracion con inicializacion
        //montaremos el subarbol de una asignacion.
        if(ConstValues.isDatatype(sentence.get(0).getType_id())){
            sentence.remove(0);
        }

        //Eliminamos el token delimitador de sentencia ';'
        int lastToken = sentence.size()-1;
        if(sentence.get(lastToken).getType_id() == ConstValues.DELIM_VALUE){
            sentence.remove(lastToken);
        }

        //Empezamos a montar el subarbol
        ParseTreeNode subarbolSentencia = new ParseTreeNode();
        subarbolSentencia.sentenceType = sentenceType; // POR EL MOMENTO SIEMPRE VA A SER UNA ASIGNACION
        subarbolSentencia.isSentence = true;
        subarbolSentencia.element = sentence.get(1);
        subarbolSentencia.children = new ArrayList<>();

        ParseTreeNode hijoIzqSentencia = new ParseTreeNode();
        hijoIzqSentencia.element = sentence.get(0);
        subarbolSentencia.children.add(hijoIzqSentencia);

        ParseTreeNode raizActual = subarbolSentencia;
        ParseTreeNode aux = new ParseTreeNode();
        int sentenceLength = sentence.size();

        int i = 2;
        while(i < sentenceLength){
            aux = new ParseTreeNode();
            if((i + 1) == sentenceLength){
                aux.element = sentence.get(i);
                raizActual.children.add(aux);
            } else {
                //Añadimos como hijo derecho el operador
                aux.element = sentence.get(i + 1);

                //El operador es ahora la raizActual
                raizActual.children.add(aux);
                raizActual = aux;
                raizActual.children = new ArrayList<>();

                //Añadimos el hijo izquiero de la operacion
                aux = new ParseTreeNode();
                aux.element = sentence.get(i);
                raizActual.children.add(0, aux);
            }

            i += 2;

        }
        return subarbolSentencia;
    }

    public static ParseTreeNode mountIfWhileBase(ArrayList<Token> sentence){
        //borramos el 1er y el último paréntseis
        if (ConstValues.PARENTHESIS_OPENING == sentence.get(1).getType_id()){
            sentence.remove(1);
        }

        if (ConstValues.PARENTHESIS_CLOSING == sentence.get(sentence.size()-1).getType_id()){
            sentence.remove(sentence.size()-1);
        }

        ParseTreeNode ifBase = new ParseTreeNode();
        ifBase.isSentence = true;
        //nos ponemos el IF como root del subarbol
        ifBase.element = sentence.get(0);
        sentence.remove(0);
        ifBase.children = new ArrayList<>();

        //nos ponemos como hijo izquierdo del IF la cabecera de éste
        ParseTreeNode hijoIzqSentencia = operationTree(sentence);
        ifBase.children.add(hijoIzqSentencia);

        //nos ponemos como hijo de la derecha un nodo "TRUE"
        ParseTreeNode hijoDerechaSentencia = new ParseTreeNode();
        hijoDerechaSentencia.setElement(new Token(SentenceBlock.BLOCK_SUBROOT, "SUBROOT"));
        hijoDerechaSentencia.children = new ArrayList<>();
        ifBase.children.add(hijoDerechaSentencia);

        return ifBase;
    }

    /**
     * Esta funcion construye el arbol sintactico de una operacion aritmetica o logica
     * @param operation Conjunto de tokens que forman la operacion que se quiere pasar a arbol sintactico
     * @return arbol sintactico de la operación
     */
    private static ParseTreeNode operationTree(ArrayList<Token> operation){

        ParseTreeNode operationRoot = new ParseTreeNode();

        int opLen = operation.size();

        if(opLen == 1){
            operationRoot.element = operation.get(0);
            return operationRoot;
        }

        //Primero siempre cojemos los tres primeros tokens
        int i = 0;
        ParseTreeNode nodoDeInteres = operationRoot;
        ParseTreeNode aux, auxNext;

        if(ConstValues.abreParentesis(operation.get(i).getType_id())){
            //Si se abre un parentesis llamo recursivamente a esta funcion para que me devuelva
            //el arbol resultante de lo que hay dentro.
            ArrayList<Token> subOperation = new ArrayList<>();
            int innerParenthesisCount = 0;
            i++;

            do{
                Token auxToken = operation.get(i);
                if(ConstValues.abreParentesis(auxToken.getType_id())){
                    innerParenthesisCount++;
                }else if(ConstValues.cierraParentesis(auxToken.getType_id())){
                    innerParenthesisCount--;
                }
                subOperation.add(auxToken);
                i++;
            }while(innerParenthesisCount != 0 && !ConstValues.cierraParentesis(operation.get(i).getType_id()));
            operationRoot = operationTree(subOperation);
        } else {
            aux = new ParseTreeNode();
            aux.element = operation.get(i++);

            operationRoot.element = operation.get(i++);
            operationRoot.children = new ArrayList<>(2);
            operationRoot.children.add(aux);

            aux = new ParseTreeNode();
            aux.element = operation.get(i++);
            operationRoot.children.add(aux);
        }

        //Bucle principal de montaje del arbol de la operación
        while(i < opLen){

            //Pillo signo
            aux = new ParseTreeNode();
            aux.element = operation.get(i++);
            aux.children = new ArrayList<>(2);

            //Pillo siguiente token al que precede el signo.
            if(ConstValues.abreParentesis(operation.get(i).getType_id())){
                //Si se abre un parentesis llamo recursivamente a esta funcion para que me devuelva
                //el arbol resultante de lo que hay dentro.
                ArrayList<Token> subOperation = new ArrayList<>();
                int innerParenthesisCount = 0;
                i++;

                do{
                    Token auxToken = operation.get(i);
                    if(ConstValues.abreParentesis(auxToken.getType_id())){
                        innerParenthesisCount++;
                    }else if(ConstValues.cierraParentesis(auxToken.getType_id())){
                        innerParenthesisCount--;
                    }
                    subOperation.add(auxToken);
                    i++;
                }while(innerParenthesisCount != 0 && !ConstValues.cierraParentesis(operation.get(i).getType_id()));
                auxNext = operationTree(subOperation);
            } else{
                //Si es un numero lo coloco como hijo directamente
                auxNext = new ParseTreeNode();
                auxNext.element = operation.get(i);
            }

            aux.children.set(1, auxNext);

            //Ahora tengo que comprobar las prioridades del nodo que hay que añadir con la del punto de interes
            //y rotar el árbol segun lo nuevo que se haya añadido
            if(ConstValues.masPrioritario(aux.element, nodoDeInteres.element)){
                //Si la prioridad es superior o igual este nuevo nodo colgara el punto de interes
                //como hijo izquierdo y pasara a ser el nuevo punto de interes.
                aux.children.set(0, nodoDeInteres.children.get(1));
                nodoDeInteres.children.set(1, aux);
            }else{
                //De tal manera que si la prioridad es menor, la raiz del arbol pasa a ser el nuevo nodo
                //la antigua raiz su hijo izquierdo y el pdi la nueva raiz
                aux.children.set(0, operationRoot);
                nodoDeInteres = aux;
                operationRoot = aux;
            }
            i++;
        }

        return operationRoot;
    }
    //**********************************************************

}
