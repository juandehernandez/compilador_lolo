package Syntactic;

import Lexicon.ConstValues;
import Lexicon.Scanner;
import Lexicon.Token;
import Error.Error;
import Error.ErrorHandler;
import Error.SintacticError;
import Error.*;
import Semantic.Semantic;
import Symbols.SymbolTable;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class Parser{
    // Attributes
    private Scanner scanner;
    private ArrayList<Token> inConstructionSentence;
    private ArrayList<ArrayList<Token>> sentencias;
    private String sentenciaFinal;
    private ErrorHandler errorHandler;
    private Semantic semantic;
    private SymbolTable symbolTable;
    private ParseTree parseTree;

    private boolean parseOK;
    private SentenceBlock programBlock;

    public Parser(SymbolTable symbolTable, String sourceFile, ErrorHandler errorHandler) throws FileNotFoundException {

        this.errorHandler = errorHandler;
        this.scanner = new Scanner(errorHandler, sourceFile);
        this.inConstructionSentence = new ArrayList<>();
        this.sentenciaFinal = "";
        this.symbolTable = symbolTable;
        this.semantic = new Semantic(errorHandler, symbolTable);
        this.sentencias = new ArrayList<>();
        this.parseTree = new ParseTree();
    }

    //****************************************
    public boolean parse(){
        parseOK = true;

        programBlock = new SentenceBlock(SentenceBlock.BLOCK_ROOT);
        configureBlockRoot();

        boolean end = false;

        while (!end){
            Token first = null;

            try {
                // Cogemos el que será el primer token del nuevo block
                first = getFirst();
                // Intentamos parsear el bloque
                if (first == null){
                    // NO hay siguiente bloque
                    end = true;
                }else{
                    SentenceBlock block = parseBlock(first, programBlock);

                    if (block == null){
                        // Lectura terminada
                        end = true;
                    }else if (block.getType() != SentenceBlock.BLOCK_INCORRECT){
                        if (!(block.getType() == SentenceBlock.BLOCK_DECLARACION && block.getRoot().size() == 3)){
                            // montar parse tree del bloque -->
                            mountParseTree(parseTree.root, block);
                            // llamar al semantico -->
                            ParseTree.ParseTreeNode treeChild = parseTree.getRoot().getChildren().get(parseTree.getRoot().getChildren().size() - 1);
                            parseOK = parseOK && semantic.start(treeChild, block);
                        }else{
                            if (block.getType() == SentenceBlock.BLOCK_DECLARACION && block.getRoot().size() == 3){
                                /*
                                tokenVariableDeclarada.setType_id(ConstValues.convertType(sentence.get(0).getType_id()));

                                // Intentamos inserirla en la tabla de símbolos
                                boolean insertedInSymbolTambleOK = symbolTable.insertKey(tokenVariableDeclarada, tokenVariableDeclarada.getScope());
                                 */
                                block.getRoot().get(1).setType_id(ConstValues.convertType(block.getRoot().get(0).getType_id()));
                                symbolTable.insertKey(block.getRoot().get(1), block.getRoot().get(1).getScope());
                            }

                        }
                    }
                }


            } catch (IOException e) {
                // No hay más para leer (fin del fichero)
                end = true;

            } catch (Error.ErrorTypeException e) {
                parseOK = false;
            }
        }

        return parseOK;
    }
    //****************************************
    private Token getFirst() throws IOException, Error.ErrorTypeException {
        return scanner.nextToken();
    }

    private void tragaBasura() throws IOException, Error.ErrorTypeException {
        Token token = scanner.nextToken();
        while (token != null && !isDelimiter(token.getType_id())){
            token = scanner.nextToken();
        }
    }

    private boolean isGoodDeclaracio(){
        try {
            // Será true cuando sea una buena declaración
            boolean correct = isDeclaracio();

            // Obtenemos el último token encontrado
            Token lastToken = inConstructionSentence.get(inConstructionSentence.size()-1);

            // Comprovamos si el último token es el del delimitador
            // En caso de error, tragaremos basura de tokens hasta
            // dar con un punto de sincronismo
            if (correct) {

                if (isDelimiter(lastToken.getType_id())) {
                    inConstructionSentence.get(1).setType_id(ConstValues.convertType(inConstructionSentence.get(0).getType_id()));
                    return true;

                } else {
                    errorHandler.addError(new SintacticError(Scanner.getLinea(), Scanner.getFILENAME(), 2));
                    tragaBasura();
                    return false;
                }
            }else {
                tragaBasura();
                return false;
            }

        } catch (Exception e){
            return false;
        }

    }

    private boolean isGoodAsignacio(Token firstToken){

        try {

            boolean correct = isAssignacio();
            Token token = inConstructionSentence.get(inConstructionSentence.size()-1);


            if (correct) {

                if (isDelimiter(token.getType_id())) {

                    if(symbolTable.getSymbol(firstToken.getName(), firstToken.getScope()) != null){
                        firstToken.setType_id(symbolTable.getSymbol(firstToken.getName(), firstToken.getScope()).getType());
                    }
                    return true;

                } else {
                    errorHandler.addError(new SintacticError(Scanner.getLinea(), Scanner.getFILENAME(), 2));
                    tragaBasura();
                    return false;
                }
            }else {
                tragaBasura();
                return false;
            }

        } catch (Exception e){
            return false;
        }
    }

    private boolean isGoodHead(){
        try {
            // Intentamos leer el '('
            Token token = scanner.nextToken();
            inConstructionSentence.add(token);

            if (isOpeningParentesis(token.getType_id())){
                token = scanner.nextToken();
                inConstructionSentence.add(token);

                boolean correct = isOperacion(token);
                token = inConstructionSentence.get(inConstructionSentence.size()-1);

                if (correct) {
                    // Intentamos leer el ')'
                    // Revisar si esto está bien (me parece que no) -> TODO
                    if (isClosingParentesis(token.getType_id())) {
                        return true;

                    }else {
                        try {
                            errorHandler.addError(new SintacticError(Scanner.getLinea(), Scanner.getFILENAME(), 1));
                        } catch (Error.ErrorTypeException e) {
                            e.getMessage();
                        }
                    }
                }
                return false;

            }

            //TODO: llamar error respectivo
            return false;

        } catch (Exception e){
            return false;
        }
    }

    private boolean isEndBlock(Token token){
        return token.getType_id() == ConstValues.END_CODE;
    }

    private SentenceBlock parseBlock(Token first, SentenceBlock daddy) throws Error.ErrorTypeException {
        // Inicializamos la nueva sentencia de tokens como una arraylist vacía
        inConstructionSentence = new ArrayList<>();
        inConstructionSentence.add(first);
        SentenceBlock block;
        Token nextFirst = null;

        // Vemos que tipo de sentencia o bloque es
        switch (first.getType_id()){

            // Declaración
            case ConstValues.INTG_TYPEID_VALUE:
            case ConstValues.CHAR_TYPEID_VALUE:
            case ConstValues.STR_TYPEID_VALUE:
            case ConstValues.BOOL_TYPEID_VALUE:

                block = new SentenceBlock(SentenceBlock.BLOCK_DECLARACION);

                boolean correctDeclaracion = isGoodDeclaracio();

                if (correctDeclaracion){
                    // Poner padre, hijos...
                    configureBlockDeclaracioAsignacio(block, daddy);
                    // Controlar semanticamente -> TODO
                    return block;
                }
                break;


            // Asignación
            case ConstValues.NAME:

                block = new SentenceBlock(SentenceBlock.BLOCK_ASIGNACION);

                boolean correctAsignacion = isGoodAsignacio(first);

                if (correctAsignacion){
                    // Poner padre, hijos...
                    configureBlockDeclaracioAsignacio(block, daddy);
                    // Controlar semanticamente -> TODO
                    return block;
                }
                break;


            case ConstValues.IF:

                block = new SentenceBlock(SentenceBlock.BLOCK_IF);

                boolean correctIf = isGoodHead();

                if (correctIf) {
                    // Poner padre, hijos...
                    configureBlockIfWhile(block, daddy);
                    // Controlar semanticamente -> TODO

                    try {
                        // Obtenemos el primer token del siguiente bloque
                        first = getFirst();

                        // Analizaremos los bloques hasta dar con el end del if
                        while (!isEndBlock(first)) {

                            // llamada recursiva para obtener un hijo del if
                            // es decir, una llamada recursiva para cada sentencia dentro del if
                            SentenceBlock son = parseBlock(first, block);

                            if (son == null) {
                                // Nos hemos quedado sin archivo para leer
                                // Error de que no se ha cerrado el if
                                errorHandler.addError(new SintacticError(Scanner.getLinea(), Scanner.getFILENAME(), 6));
                                parseOK = false;
                                return null;

                                // Si es un bloque correcto
                            } else {
                                //if (son.getType() != SentenceBlock.BLOCK_INCORRECT){
                                    // Añadimos sentencia hijo y seguimos a por el próximo hijo
                                  //  block.getBlocks().add(son);
                                //}
                                // Buscamos el primer token del proximo hijo
                                first = getFirst();
                                //first = inConstructionSentence.get(inConstructionSentence.size()-1);
                            }
                        }
                        // IF completamente correcto
                        return block;
                    } catch (IOException e) {
                        // Error de que no se ha cerrado el if
                        errorHandler.addError(new SintacticError(Scanner.getLinea(), Scanner.getFILENAME(), 6));
                        parseOK = false;
                        return null;
                    }
                }
                break;


            case ConstValues.WHILE:
                block = new SentenceBlock(SentenceBlock.BLOCK_WHILE);

                boolean correctWhile = isGoodHead();

                if (correctWhile) {
                    // Poner padre, hijos...
                    configureBlockIfWhile(block, daddy);

                    try {
                        // Obtenemos el primer token del siguiente bloque
                        first = getFirst();

                        // Analizaremos los bloques hasta dar con el end del if
                        while (!isEndBlock(first)) {

                            // llamada recursiva para obtener un hijo del if
                            // es decir, una llamada recursiva para cada sentencia dentro del if
                            SentenceBlock son = parseBlock(first, block);

                            if (son == null) {
                                // Nos hemos quedado sin archivo para leer
                                // Error de que no se ha cerrado el if
                                errorHandler.addError(new SintacticError(Scanner.getLinea(), Scanner.getFILENAME(), 6));
                                parseOK = false;
                                return null;

                                // Si es un bloque correcto
                            } else {
                                //if (son.getType() != SentenceBlock.BLOCK_INCORRECT){
                                // Añadimos sentencia hijo y seguimos a por el próximo hijo
                                //  block.getBlocks().add(son);
                                //}
                                // Buscamos el primer token del proximo hijo
                                first = getFirst();
                                //first = inConstructionSentence.get(inConstructionSentence.size()-1);
                            }
                        }
                        // IF completamente correcto
                        return block;
                    } catch (IOException e) {
                        // Error de que no se ha cerrado el if
                        errorHandler.addError(new SintacticError(Scanner.getLinea(), Scanner.getFILENAME(), 6));
                        parseOK = false;
                        return null;
                    }
                }
                break;


            default:

                try {
                    errorHandler.addError(new SintacticError(Scanner.getLinea(), Scanner.getFILENAME(), 3));
                } catch (Error.ErrorTypeException e) {
                    parseOK = false;
                }

                break;
        }

        parseOK = false;
        return new SentenceBlock(SentenceBlock.BLOCK_INCORRECT);
    }

    // Configuracion de bloques
    private void configureBlockDeclaracioAsignacio(SentenceBlock block, SentenceBlock daddy) {
        // Configuramos el bloque
        block.setRoot(inConstructionSentence);
        block.setBlocks(null);

        // Ponemos el bloque como hijo de daddy
        daddy.getBlocks().add(block);
    }
    private void configureBlockIfWhile(SentenceBlock block, SentenceBlock daddy){
        // Configuramos el bloque
        block.setRoot(inConstructionSentence);
        block.setBlocks(new ArrayList<SentenceBlock>());

        // Ponemos el bloque como hijo de daddy
        daddy.getBlocks().add(block);
    }
    private void configureBlockRoot(){
        programBlock.setRoot(null);
        programBlock.setBlocks(new ArrayList<SentenceBlock>());
    }

    // Parse tree después de comprovar que el programa es correcto sintácticamente
    public void createParseTree() {
        // Creamos el parse tree
        parseTree = new ParseTree();
        // Configuramos el nodo root
        ParseTree.ParseTreeNode daddy = new ParseTree.ParseTreeNode();
        daddy.sentenceType = SentenceBlock.BLOCK_ROOT;
        daddy.isSentence = false;
        daddy.children = new ArrayList<ParseTree.ParseTreeNode>();

        // añadimos cada bloque como hijos (llamadas recursivas para hijos de hijos)
        int numBlocks = programBlock.getBlocks().size();
        for (int i = 0; i < numBlocks; i++) {
            // Get the block
            SentenceBlock block = programBlock.getBlocks().get(i);
            // mount parse tree for the specified block --> TODO
            mountParseTree(daddy, block);


        }
        // Añadimos el nodo root como elemento root del árbol
        parseTree.root = daddy;
    }


    public void mountParseTree(ParseTree.ParseTreeNode daddy, SentenceBlock blockToMount){
        switch (blockToMount.getType()){
            // Declaración o asignación
            case SentenceBlock.BLOCK_DECLARACION:
            case SentenceBlock.BLOCK_ASIGNACION:

                boolean correct = true;
                // Cogemos la sentencia de la declaración
                ArrayList<Token> sentence = blockToMount.getRoot();

                if (blockToMount.getType() == SentenceBlock.BLOCK_DECLARACION){
                    // Obtenemos el token del nombre de la variable que declaramos
                    Token tokenVariableDeclarada = sentence.get(1);

                    // Convertimos el tipo de la variable por el tipo que le precede
                    tokenVariableDeclarada.setType_id(ConstValues.convertType(sentence.get(0).getType_id()));

                    // Intentamos inserirla en la tabla de símbolos
                    boolean insertedInSymbolTambleOK = symbolTable.insertKey(tokenVariableDeclarada, tokenVariableDeclarada.getScope());
                    if (!insertedInSymbolTambleOK){
                        // Error --> variable declarada anteriormente
                        addPreviousDeclaredVariableError(tokenVariableDeclarada);
                        correct = false;
                    }
                }

                if (correct){
                    ParseTree.ParseTreeNode child = ParseTree.mountAsignationDeclaration(sentence);
                    // Maybe look if semantic is correct
                    daddy.children.add(child);

                }
                break;


            case SentenceBlock.BLOCK_IF:
            case SentenceBlock.BLOCK_WHILE:
                // Obtenemos la cabecera del IF
                ArrayList<Token> cabeceraIF = blockToMount.getRoot();
                // Montamos la base del arbol del if
                ParseTree.ParseTreeNode ifOrWhileBase = ParseTree.mountIfWhileBase(cabeceraIF);
                ParseTree.ParseTreeNode subrootNode = ifOrWhileBase.children.get(1);

                // Montamos todos los bloques hijos (dentro) del if colgando del subroot node
                int numChildBlocks = blockToMount.getBlocks().size();
                for (int i = 0; i < numChildBlocks; i++){
                    SentenceBlock childBlock = blockToMount.getBlocks().get(i);
                    mountParseTree(subrootNode, childBlock);
                }

                // LLAMAR A SEMANTICO PARA EL IF --> TODO

                // Montamos el arbol del if al daddy
                daddy.children.add(ifOrWhileBase);

                break;

        }
    }

    private void addPreviousDeclaredVariableError(Token token) {
        try {
            errorHandler.addError(new SemanticError(token.getFileLine(), token.getFile(), 0));
        } catch (Error.ErrorTypeException e) {
            e.getMessage();
        }
    }

    //****************************************

    public ParseTree getParseTree() {
        return parseTree;
    }


    private boolean isDeclaracio() throws IOException, Error.ErrorTypeException {

        boolean correct = true;
        Token token = scanner.nextToken();
        inConstructionSentence.add(token);
        sentenciaFinal += " " + token.getName();
        //System.out.println(sentenciaFinal);
        int id = token.getType_id();
        // TODO: Añadir a lista para semantico

        if (isVariable(id)){
            token = scanner.nextToken();
            inConstructionSentence.add(token);
            sentenciaFinal += " " + token.getName();
            //System.out.println(sentenciaFinal);
            id = token.getType_id();

            if (isIgualAssignacio(id)){
                token = scanner.nextToken();
                inConstructionSentence.add(token);
                sentenciaFinal += " " + token.getName();
                //System.out.println(sentenciaFinal);
                correct = isOperacion(token);
            }else {
                correct = true; // Fuera comprovamos si es un ; o no lo es

            }

        }else{
            correct = false;
            try {
                errorHandler.addError(new SintacticError(Scanner.getLinea(), Scanner.getFILENAME(), 4));
            } catch (Error.ErrorTypeException e) {
                e.getMessage();
            }
        }
        return correct;
    }


    private boolean isAssignacio() throws IOException, Error.ErrorTypeException {
        boolean correct;
        Token token = scanner.nextToken();
        inConstructionSentence.add(token);
        sentenciaFinal += " " + token.getName();
        //System.out.println(sentenciaFinal);
        int id = token.getType_id();
        // TODO: Añadir a lista para semantico

        if (isIgualAssignacio(id)){
            token = scanner.nextToken();
            inConstructionSentence.add(token);
            sentenciaFinal += " " + token.getName();
            //System.out.println(sentenciaFinal);
            correct = isOperacion(token);

        }else{
            correct = false;
            try {
                errorHandler.addError(new SintacticError(Scanner.getLinea(), Scanner.getFILENAME(), 4));
            } catch (Error.ErrorTypeException e) {
                e.getMessage();
            }
        }

        return correct;
    }


    private boolean isOperacion(Token token2) throws IOException, Error.ErrorTypeException {
        boolean correct = true;
        Token token;
        int id = token2.getType_id();

        if (isVariable(id) || isLiteral(id)){
            if (isVariable(id)){
                if (symbolTable.getSymbol(token2.getName(), token2.getScope()) != null) {
                    token2.setType_id(symbolTable.getSymbol(token2.getName(), token2.getScope()).getType());
                }
//                } else {
//                    errorHandler.addError(new SemanticError(token2.getFileLine(), token2.getFile(), 1));
//                    correct = false;
//                }
                correct = true;
            }
            token = scanner.nextToken();
            inConstructionSentence.add(token);
            sentenciaFinal += " " + token.getName();
            //System.out.println(sentenciaFinal);
            id = token.getType_id();

            if (isMathOperation(id) || isEq(id) || isComp(id)){
                token = scanner.nextToken();
                inConstructionSentence.add(token);
                sentenciaFinal += " " + token.getName();
                //System.out.println(sentenciaFinal);
                correct = isOperacion(token);

            }

        }else if (isOpeningParentesis(id)){
            token = scanner.nextToken();
            inConstructionSentence.add(token);
            sentenciaFinal += " " + token.getName();
            //System.out.println(sentenciaFinal);
            correct = isOperacion(token);
            token = inConstructionSentence.get(inConstructionSentence.size()-1);

            if (isClosingParentesis(token.getType_id())) {
                token2 = scanner.nextToken();
                inConstructionSentence.add(token2);
                sentenciaFinal += " " + token2.getName();
                //System.out.println(sentenciaFinal);
                id = token2.getType_id();

                if (isMathOperation(id)){
                    token2 = scanner.nextToken();
                    inConstructionSentence.add(token2);
                    sentenciaFinal += " " + token2.getName();
                    //System.out.println(sentenciaFinal);
                    correct = isOperacion(token2);
                }

            }else{
                try {
                    errorHandler.addError(new SintacticError(Scanner.getLinea(), Scanner.getFILENAME(), 1));
                } catch (Error.ErrorTypeException e) {
                    e.getMessage();
                }
                correct = false;
            }
        }else{
            correct = false;
            try {
                errorHandler.addError(new SintacticError(Scanner.getLinea(), Scanner.getFILENAME(), 4));
            } catch (Error.ErrorTypeException e) {
                e.getMessage();
            }
        }

        return correct;
    }

    private boolean isComp(int id) {
        /*
        public static final int EQ = 30; // eq
    public static final int GT = 31; // gt
    public static final int LT = 32; // lt
    public static final int DIF = 33; // dif
    public static final int GTEQ = 34; // gteq
    public static final int LTEQ = 35; // lteq
         */
        return id == ConstValues.EQ || id == ConstValues.GT || id == ConstValues.LT
                || id == ConstValues.DIF || id == ConstValues.GTEQ || id == ConstValues.LTEQ;
    }

    private boolean isExpresionBooleana(Token token) {

        // **********************************************
        return false;
    }

    private boolean isDelimiter(int id) {
        return id == ConstValues.DELIM_VALUE;
    }


    private boolean isLiteral(int id){
        return (id == ConstValues.INTG_VALUE
                || id == ConstValues.CHAR_VALUE
                || id == ConstValues.STR_VALUE
                || id == ConstValues.BOOL_VALUE);
    }

    private boolean isVariable(int id){
        return (id == ConstValues.NAME);
    }

    private boolean isIgualAssignacio(int id) {
        return id == ConstValues.ASSIGN_EQUAL_VALUE;
    }

    private boolean isOpeningParentesis(int id) {
        return id == ConstValues.PARENTHESIS_OPENING;
    }

    private boolean isMathOperation(int id){
        return id == ConstValues.MATH_OPERATION_VALUE;
    }

    private boolean isClosingParentesis(int id){
        return id == ConstValues.PARENTHESIS_CLOSING;
    }

    private boolean isOpeningFunction(int id) {
        return id == ConstValues.FUNCTION_OPENING;
    }

    private boolean isSaltoLinea(int id){
        return id == ConstValues.END_LINE;
    }

    private boolean isEq(int id){
        return id == ConstValues.EQ;
    }


}
