package Semantic;

import Error.*;
import Error.Error;
import Lexicon.ConstValues;
import Lexicon.Token;
import Symbols.SymbolTable;
import Syntactic.ParseTree;
import Syntactic.SentenceBlock;


public class Semantic extends Thread {
    private ErrorHandler errorHandler;
    private Token firstVariable;
    private SymbolTable symbolTable;

    public Semantic(ErrorHandler errorHandler, SymbolTable symbolTable) {
        this.symbolTable = symbolTable;
        this.errorHandler = errorHandler;
    }

    /**
     * procedimiento qeu empieza el análisis semántico
     * @param arbol sentencia a analizar
     *  booleano que indica si la sentencia es una declaración o asignación
     */
    public boolean start(ParseTree.ParseTreeNode arbol, SentenceBlock block){
        //System.out.println("\n---------- EMPEZAMOS ANÁLISIS SEMANTICO ----------\n");
        int wasBefore = 0;
        boolean correct = false;

        //sentenceType = arbol.getSentenceType();
        int sentenceType = block.getType();

        if (sentenceType != 3 && sentenceType != 4){
            this.firstVariable = arbol.getChildren().get(0).getElement();
            wasBefore = symbolTable.existsKey(firstVariable.getName(), firstVariable.getScope());
        }else {
            this.firstVariable = arbol.getChildren().get(0).getChildren().get(0).getElement();
        }



        switch (sentenceType){
            //declaración
            case 1:
                // Insert new variable to Symbol table
                // If it was declared before in the same scope it will be an error
                //if (wasBefore == -4 || wasBefore == -3) {
                    //metemos variable en la tabla
                    //symbolTable.insertKey(firstVariable, firstVariable.getScope());
                    correct = consistenciaSentencia(arbol.getChildren().get(1));
                /*} else {

                    //System.out.println("La variable ya había sido declarada");
                    try {
                        errorHandler.addError(new SemanticError(firstVariable.getFileLine(), firstVariable.getFile(), 0));
                    } catch (Error.ErrorTypeException e) {
                        e.getMessage();
                    }
                    correct = false;
                }*/
                break;

            //asignación
            case 2:
                if (wasBefore == -4 || wasBefore == -3) {
                    //System.out.println("error variable no declarada");
                    try {
                        errorHandler.addError(new SemanticError(firstVariable.getFileLine(), firstVariable.getFile(), 1));
                    } catch (Error.ErrorTypeException e) {
                        e.getMessage();
                    }
                    correct = false;

                } else {
                    //System.out.println("comprobamos que ya ha sido declarada!");
                    correct = consistenciaSentencia(arbol.getChildren().get(1));
                }
                break;

             //if
            case 3:

                correct = consistenciaSentencia(arbol.getChildren().get(0));

                if (correct){
                    for (int i = 0; i < arbol.getChildren().get(1).getChildren().size(); i++){
                        start(arbol.getChildren().get(1).getChildren().get(i), block.getBlocks().get(i));
                    }
                }

                break;

            //while
            case 4:

                correct = consistenciaSentencia(arbol.getChildren().get(0));

                if (correct){
                    for (int i = 0; i < arbol.getChildren().get(1).getChildren().size(); i++){
                        start(arbol.getChildren().get(1).getChildren().get(i), block.getBlocks().get(i));
                    }
                }

            break;
        }


        return correct;
    }

    /**
     * recorremos el árbol mirando entero mirando si la variable coincide con el resto de lementos asignados
     * @param arbol sentencia a analizar
     * @return devuelve un booleano que indica si la sentencia es correcta o no
     */
    private boolean consistenciaSentencia(ParseTree.ParseTreeNode arbol){
        //System.out.println("miramos su consistencia");
        boolean correct = true;
        Token token;
        SymbolTable.Symbol symbol, verifyingSymbol;
        SymbolTable.Symbol symbol2 = null;

        int id;
        boolean end = false;


        if (firstVariable.getScope() == ConstValues.GLOBAL_SCOPE) {
            symbol2 = (SymbolTable.Symbol) symbolTable.getGlobalScope().get(firstVariable.getName());
        }else if (firstVariable.getScope() == ConstValues.MAIN_SCOPE){
            symbol2 = (SymbolTable.Symbol) symbolTable.getGlobalScope().get(firstVariable.getName());
        }else{
            correct = false;
            end = true;
        }

        while (!end && correct){
            token = arbol.getElement();
            //System.out.println("token mirando: " + token.getName());

            if (token.getScope() == ConstValues.GLOBAL_SCOPE) {
                symbol = (SymbolTable.Symbol) symbolTable.getGlobalScope().get(token.getName());
            }else if (token.getScope() == ConstValues.MAIN_SCOPE){
                symbol = (SymbolTable.Symbol) symbolTable.getGlobalScope().get(token.getName());
            }else{
                try {
                    errorHandler.addError(new SemanticError(firstVariable.getFileLine(), firstVariable.getFile(), 1));
                } catch (Error.ErrorTypeException e) {
                    e.getMessage();
                }
                break;
            }

            id = token.getType_id();
            switch (id){
                case ConstValues.MATH_OPERATION_VALUE:
                case ConstValues.ASSIGN_EQUAL_VALUE:
                case ConstValues.EQ:
                case ConstValues.LT:
                    //miramos el siguiente token
                    correct = consistenciaSentencia(arbol.getChildren().get(0));

                    if (correct){
                        correct = consistenciaSentencia(arbol.getChildren().get(1));
                        end = true;
                    }
                    break;

                case ConstValues.NAME: //varaible
                    if(symbol != null && symbol2.getType() == symbol.getType()){
                        if (arbol.getChildren() == null){
                            //System.out.println("acabamos rama!");
                            correct = true;
                        }else{
                            correct = false;
                        }
                        end = true;
                    }else{
                        int wasBefore = symbolTable.existsKey(token.getName(), token.getScope());
                        if (wasBefore == -4 || wasBefore == -3) {
                            //variable no declarada
                            try {
                                errorHandler.addError(new SemanticError(firstVariable.getFileLine(), firstVariable.getFile(), 1));
                            } catch (Error.ErrorTypeException e) {
                                e.getMessage();
                            }
                        }else {
                            //System.out.println("ERROR! tipo de variable errónea");
                            try {
                                errorHandler.addError(new SemanticError(firstVariable.getFileLine(), firstVariable.getFile(), 2));
                            } catch (Error.ErrorTypeException e) {
                                e.getMessage();
                            }
                        }
                        correct = false;
                        end = true;
                    }
                    break;

                case ConstValues.BOOL_VALUE:
                    if(symbol2.getType() == ConstValues.BOOL_VALUE){
                        if (arbol.getChildren() == null){
                            //System.out.println("acabamos rama!");
                            correct = true;
                        }else{
                            correct = false;
                        }
                        end = true;
                    }else{
                        try {
                            errorHandler.addError(new SemanticError(firstVariable.getFileLine(), firstVariable.getFile(), 2));
                        } catch (Error.ErrorTypeException e) {
                            e.getMessage();
                        }
                        end = true;
                        correct = false;
                    }
                    break;

                case ConstValues.INTG_VALUE:
                    if(symbol2.getType() == ConstValues.INTG_VALUE){
                        if (arbol.getChildren() == null){
                            //System.out.println("acabamos rama!");
                            correct = true;
                        }else{
                            correct = false;
                        }
                        end = true;
                    }else{
                        try {
                            errorHandler.addError(new SemanticError(firstVariable.getFileLine(), firstVariable.getFile(), 2));
                        } catch (Error.ErrorTypeException e) {
                            e.getMessage();
                        }
                        end = true;
                        correct = false;
                    }
                    break;

                case ConstValues.CHAR_VALUE:
                    if(symbol2.getType() == ConstValues.CHAR_VALUE
                            || symbol2.getType() == ConstValues.STR_VALUE){
                        if (arbol.getChildren() == null){
                            //System.out.println("acabamos rama!");
                            correct = true;
                        }else{
                            correct = false;
                        }
                        end = true;
                    }else{
                        try {
                            errorHandler.addError(new SemanticError(firstVariable.getFileLine(), firstVariable.getFile(), 2));
                        } catch (Error.ErrorTypeException e) {
                            e.getMessage();
                        }
                        end = true;
                        correct = false;
                    }

                    break;
                case ConstValues.STR_VALUE:
                    if(symbol2.getType() == ConstValues.STR_VALUE){
                        if (arbol.getChildren() == null){
                            //System.out.println("acabamos rama!");
                            correct = true;
                        }else{
                            correct = false;
                        }
                        end = true;
                    }else{
                        try {
                            errorHandler.addError(new SemanticError(firstVariable.getFileLine(), firstVariable.getFile(), 2));
                        } catch (Error.ErrorTypeException e) {
                            e.getMessage();
                        }
                        end = true;
                        correct = false;
                    }
                    break;

                default:
                    end = true;
                    correct = false;
                    break;

            }
        }

        return correct;
    }


/*
    private boolean isDelimiter(int id) {
        return id == ConstValues.DELIM_VALUE;
    }

    private boolean isLiteral(int id){
        return (id == ConstValues.INTG_VALUE
                || id == ConstValues.CHAR_VALUE
                || id == ConstValues.STR_VALUE
                || id == ConstValues.BOOL_VALUE);
    }

    private boolean isVariable(int id){ return (id == ConstValues.NAME); }

    private boolean isIgualAssignacio(int id) {
        return id == ConstValues.ASSIGN_EQUAL_VALUE;
    }

    private boolean isOpeningParentesis(int id) {
        return id == ConstValues.PARENTHESIS_OPENING;
    }

    private boolean isMathOperation(int id){
        return id == ConstValues.MATH_OPERATION_VALUE;
    }

    private boolean isClosingParentesis(int id){
        return id == ConstValues.PARENTHESIS_CLOSING;
    }

    private boolean isBoolean(int id){
        return id == ConstValues.BOOL_VALUE;
    }

    private boolean isNumber(int id){
        return id == ConstValues.INTG_VALUE;
    }

    private boolean isCharacter(int id){
        return id == ConstValues.CHAR_VALUE;
    }

    private boolean isString(int id){
        return id == ConstValues.STR_VALUE;
    }
*/
}

