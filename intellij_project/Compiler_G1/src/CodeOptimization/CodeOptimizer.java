package CodeOptimization;

import IntermediateCode.IntermediateTable;
import IntermediateCode.IntermediateTableEntry;
import IntermediateCode.RegisterToken;
import Lexicon.ConstValues;
import Lexicon.Token;
import Mips.MipsGenerator;
import Symbols.SymbolTable;
import Syntactic.ParseTree;

import java.util.List;

/**
 * Es la clase que se encarga de optimizar el código.
 * Recibe el ParseTree y genera una tabla de código intermedio.
 * Con esta tabla se puede hacer una optimización de código de manera sencilla (por cada línea de la tabla, dados dos operandos se aplica una optimización).
 * Optimizaciones tales como Constant Propagation y Constant Folding.
 * Al acabar con las optimizaciones, se llama a la generación de código mips.
 */
public class CodeOptimizer {
    // Attributes
    private ParseTree programTree;
    private IntermediateTable intermediateTable;
    private SymbolTable symbolTable;
    private MipsGenerator mipsGenerator;
    //Variable para tener el último resultado de una operación para
    private int lastResult;

    public IntermediateTable getIntermediateTable() {
        return intermediateTable;
    }

    /**
     * Constructor que llama a la optimización de código y a la generación de código mips.
     * @param parseTree Arbol de sentencias.
     * @param symbolTable Tabla de símbolos para guardar valores de una variable y también obtenerlos cuando hay que propagar una constante.
     */
    public CodeOptimizer(ParseTree parseTree, SymbolTable symbolTable){
        this.programTree = parseTree;
        this.symbolTable = symbolTable;
        /*intermediateTable = new IntermediateTable(symbolTable);
        intermediateTable.addEntry(new Token(ConstValues.INTG_VALUE, "5"), new Token(ConstValues.INTG_VALUE, "a"), new Token(ConstValues.EQ, "="));
        intermediateTable.addEntry(new Token(ConstValues.INTG_VALUE, "6"), new Token(ConstValues.INTG_VALUE, "b"), new Token(ConstValues.EQ, "="));
        intermediateTable.addEntry(new Token(ConstValues.INTG_VALUE, "7"), new Token(ConstValues.INTG_VALUE, "c"), new Token(ConstValues.EQ, "="));
        IntermediateTable intermediateTable2 = parseTree.generateIntermediateCode(symbolTable);

        for (int i = 0; i < intermediateTable2.size(); i++) {
            intermediateTable.addEntry(intermediateTable2.getEntries().get(i).getOP1(), intermediateTable2.getEntries().get(i).getOP2(), intermediateTable2.getEntries().get(i).getOp());
        }

        symbolTable.insertKey(new Token(ConstValues.INTG_VALUE, "a"), -2);
        symbolTable.getSymbol("a", -2).setValue("5");
        symbolTable.insertKey(new Token(ConstValues.INTG_VALUE, "b"), -2);
        symbolTable.getSymbol("b", -2).setValue("6");
        symbolTable.insertKey(new Token(ConstValues.INTG_VALUE, "c"), -2);
        symbolTable.getSymbol("c", -2).setValue("7");
         */

       intermediateTable = parseTree.generateIntermediateCode(symbolTable);
        this.mipsGenerator = new MipsGenerator(intermediateTable, symbolTable);
        lastResult = -1;
    }

    public MipsGenerator getMipsGenerator(){return mipsGenerator;}

    /**
     * Método que llama a las funciones de optimización (doConstantPropagation(), doConstantFolding()...).
     * Las llama en un bucle que acabará cuando la ultima función de optimización devuelva "false" porque no se ha hecho una optimización.
     */
    public void optimize() {
        //Variable para saber si se ha aplicado optimización.
        //Cuando no se aplique optimización se dejará de hacer saliendo del bucle.
        System.out.println("INITIAL TABLE");
        System.out.println("Intermediate table entries are:");
        for (int i = 0; i < intermediateTable.size(); i++) {
            System.out.println(intermediateTable.getEntries().get(i).getOP1().getName() + " " + intermediateTable.getEntries().get(i).getOp().getName() + " " + intermediateTable.getEntries().get(i).getOP2().getName());
        }
        boolean optimization = true;
        while (optimization) {

            //DO CONSTANT PROPAGATION
            doConstantPropagation();
            System.out.println("Intermediate table entries are:");
            for (int i = 0; i < intermediateTable.size(); i++) {
                System.out.println(intermediateTable.getEntries().get(i).getOP1().getName() + " " + intermediateTable.getEntries().get(i).getOp().getName() + " " + intermediateTable.getEntries().get(i).getOP2().getName());
            }

            //DO CONSTANT FOLDING
            doConstantFolding();
            System.out.println("Intermediate table entries are:");
            for (int i = 0; i < intermediateTable.size(); i++) {
                System.out.println(intermediateTable.getEntries().get(i).getOP1().getName() + " " + intermediateTable.getEntries().get(i).getOp().getName() + " " + intermediateTable.getEntries().get(i).getOP2().getName());
            }

            optimization = doBlockElimination();

            System.out.println("Intermediate table entries after block elimination are:");
            for (int i = 0; i < intermediateTable.size(); i++) {
                System.out.println(intermediateTable.getEntries().get(i).getOP1().getName() + " " + intermediateTable.getEntries().get(i).getOp().getName() + " " + intermediateTable.getEntries().get(i).getOP2().getName());
            }
        }

        System.out.println("Final Intermediate table entries are:");
        for (int i = 0; i < intermediateTable.size(); i++) {
            System.out.println(intermediateTable.getEntries().get(i).getOP1().getName() + " " + intermediateTable.getEntries().get(i).getOp().getName() + " " + intermediateTable.getEntries().get(i).getOP2().getName());
        }
    }

    /**
     *  Función para saber cual es el resultado (true o false) de una comparación dado dos enteros y un token de operador (operadores: eq, gt, lt...)
     */
    private boolean getComparation(Token op, int op1, int op2) {
        switch (op.getType_id()) {
            case ConstValues.EQ:
                return op1 == op2;
            case ConstValues.GT:
                return op2 > op1;
            case ConstValues.LT:
                return op2 < op1;
            case ConstValues.GTEQ:
                return op2 >= op1;
            case ConstValues.LTEQ:
                return op2 <= op1;
            default:
                return false;
        }
    }

    /**
     * Función que dado dos operandos y una operación, da el resultado.
     * Es usado para Constant Folding (2+2 = 4 directamente)
     * @param op1 Operando 1 de una entrada de la tabla de código intermedio
     * @param op2 Operando 2 de una entrada de la tabla de código intermedio
     * @param op Operación que se aplica en los dos operandos.
     * @return Devuelve el resultado de la operación en tipo int
     */
    private int getResult(Token op1, Token op2, Token op) {
        int operand1 = Integer.parseInt(op1.getName());
        int operand2 = Integer.parseInt(op2.getName());
        //System.out.println("Estoy recibiendo operando 1 como "+operand1 + " y operando 2 como "+ operand2 + " y la operancion entre estos dos es "+op.getName());
        /*if (lastResult != -1) {
            operand1 = lastResult;
        } else {
            operand1 = Integer.parseInt(op1.getName());
        }*/
        Token tokenOPERADOR = op;
        //System.out.println("vamos a hacer el switch segun " + tokenOPERADOR.getName());
        switch (tokenOPERADOR.getName()) {
            case "+":
                //System.out.println("HACEMOS SUMITA");
                lastResult = operand2 + operand1;
                break;
            case "-":
                lastResult = operand2 - operand1;
                break;
            case "*":
                lastResult = operand2 * operand1;
                break;
            case "/":
                lastResult = operand2 / operand1;
                break;
        }

        //System.out.println("voy a retornar un last result de "+lastResult);
        return lastResult;
    }

    /**
     * Función que se encarga de eliminar los bloques IF o WHILE innecesarios (No se cumple condición o no tiene ninguna sentencia dentro)
     */
    private boolean doBlockElimination() {
        //Variable para saber si se ha aplicado optimización.
        //Cuando no se aplique optimización se dejará de hacer.
        boolean optimization = false;

        List<IntermediateTableEntry> entries = intermediateTable.getEntries();
        for (int i = 0; i < entries.size(); i++) {
            if (entries.get(i).getOp().getType_id() == ConstValues.IF) {
                //System.out.println("El valor del IF endline es: " + entries.get(Integer.parseInt(entries.get(i).getR().getName()) - 1).getOp().getName());
                if (entries.get(i).getOP1().getName().equals("false")) {
                    System.out.println("ELIMINAMOS");
                    boolean elimina = true;
                    int pos = i;

                    while (pos < Integer.parseInt(entries.get(i).getR().getName())) {
                        intermediateTable.deleteEntry(pos);
                        pos++;
                    }

                    optimization = true;
                } else {
                    entries.remove(i);
                }
            } else if (entries.get(i).getOp().getType_id() == ConstValues.WHILE) {
                System.out.println("is while");
                if (entries.get(i).getOP1().getName().equals("false")) {
                    System.out.println("ELIMINAMOS");
                    boolean elimina = true;
                    int pos = i;
                    while (elimina) {
                        if (entries.get(pos).getOp().getName().equals("endwhile")) {
                            elimina = false;
                        }
                        intermediateTable.deleteEntry(pos);
                    }

                    optimization = true;
                }
            }
        }

        return optimization;
    }

    /**
     * Función de optimización que propaga las constantes.
     * Para cada entrada de la tabla de código intermedio, se comprueba si hay una variable y si es así,
     * se coge el valor de la tabla de símbolos (si hay valor final) y se actualiza la tabla de código intermedio con este nuevo valor.
     */
    private void doConstantPropagation() {
        //DO CONSTANT PROPAGATION
        List<IntermediateTableEntry> entries = intermediateTable.getEntries();

        int countWhiles = 0;
        for (int i = 0; i < entries.size(); i++) {
            if (entries.get(i).getOp().getName().equals("initwhile")) {
                countWhiles++;
            } else if (entries.get(i).getOp().getName().equals("endwhile")) {
                countWhiles--;
            }

            if (countWhiles > 0) {
                continue;
            }
            System.out.println("Estamos en la linea " + entries.get(i).getOp().getName());
            //Si la entrada tiene un igual (=), aprovecharemos para ver si podemos poner valor a una variable en la tabla de símbolos,
            //cosa que nos irá muy bien para hacer una propagación (si encontramos variable podremos propagarla si tenemos asignado un valor final)
            if (entries.get(i).getOp().getType_id() == ConstValues.ASSIGN_EQUAL_VALUE ) {
                boolean numeric = true;
                try {
                    Integer num = Integer.parseInt(entries.get(i).getOP1().getName());
                } catch (NumberFormatException e) {
                    numeric = false;
                }
                if (numeric) {
                    symbolTable.getSymbol(entries.get(i).getOP2().getName(), -2).setValue(entries.get(i).getOP1().getName());
                }

                //Aprovechamos para propagar si a una variable se le asigna el valor de otra variable.

                //Si es una variable (existe en la tabla de símbolos)
                if (symbolTable.existsKey(entries.get(i).getOP1().getName(), -2) == -2
                        && symbolTable.getSymbol(entries.get(i).getOP1().getName(), -2).getValue() != null) {
                    System.out.println("Vamos a escribir en la tabla de simbolos la variable" + entries.get(i).getOP2().getName());
                    //Entonces obtenemos su valor y actualizamos la entrada en la Intermediate Table
                    //TODO parece ser que habra que poner OP2 en la siguiente linea -> entries.get(i).getOP2().setName(symbolTable.getSymbol(entries.get(i).getOP1().getName(), -2).getValue());
                    entries.get(i).getOP2().setName(symbolTable.getSymbol(entries.get(i).getOP1().getName(), -2).getValue());
                }
            }

            //Si la entrada tiene operación matemática veremos si se puede hacer un constant propagation en alguno o en ambos operandos
            if (entries.get(i).getOp().getType_id() == ConstValues.MATH_OPERATION_VALUE
                    || entries.get(i).getOp().getType_id() == ConstValues.LT
                    || entries.get(i).getOp().getType_id() == ConstValues.GT
                    || entries.get(i).getOp().getType_id() == ConstValues.LTEQ
                    || entries.get(i).getOp().getType_id() == ConstValues.GTEQ
                    || entries.get(i).getOp().getType_id() == ConstValues.EQ) {

                System.out.println("Vamos a intentar propagar");
                //Miramos si el operando 1 es una variable que está en la tabla de simbolos
                System.out.println("El exists key retorna " + symbolTable.existsKey(entries.get(i).getOP1().getName(), -2));
                if (symbolTable.existsKey(entries.get(i).getOP1().getName(), -2) == -2) {
                    //Si lo está, vemos si tiene un valor o en cambio si es null. Si tiene valor entonces lo intercambiamos directamente.
                    if (symbolTable.getSymbol(entries.get(i).getOP1().getName(), -2).getValue() != null) {
                        //Le ponemos el valor de la variable directamente cogiéndola de la tabla de símbolos
                        entries.get(i).getOP1().setName(symbolTable.getSymbol(entries.get(i).getOP1().getName(), -2).getValue());
                        //Le ponemos tipo intg porque ya no es tipo variable
                        entries.get(i).getOP1().setType_id(ConstValues.INTG_VALUE);
                        //System.out.println("La entrada es " + entries.get(i).getOP1().getName() + " " + entries.get(i).getOp().getName() + " " + entries.get(i).getOP2().getName());
                    }
                }
                //Miramos si el operando 2 es una variable que está en la tabla de simbolos
                if (symbolTable.existsKey(entries.get(i).getOP2().getName(), -2) == -2) {
                    //Si lo está, vemos si tiene un valor o en cambio si es null. Si tiene valor entonces lo intercambiamos directamente.
                    if (symbolTable.getSymbol(entries.get(i).getOP2().getName(), -2).getValue() != null) {
                        //Le ponemos el valor de la variable directamente cogiéndola de la tabla de símbolos
                        entries.get(i).getOP2().setName(symbolTable.getSymbol(entries.get(i).getOP2().getName(), -2).getValue());
                        //Le ponemos tipo intg porque ya no es tipo variable
                        entries.get(i).getOP2().setType_id(ConstValues.INTG_VALUE);
                        /*System.out.println("La entrada es " + entries.get(i).getOP1().getName() + " " + entries.get(i).getOp().getName() + " " + entries.get(i).getOP2().getName());
                        System.out.println("Intermediate table 2 entries are:");
                        for (int j = 0; j < intermediateTable.size(); j++) {
                            System.out.println(intermediateTable.getEntries().get(j).getOP1().getName() + " " + intermediateTable.getEntries().get(j).getOp().getName() + " " + intermediateTable.getEntries().get(j).getOP2().getName());
                        }*/
                    }
                }

            } else if (entries.get(i).getOp().getType_id() == ConstValues.LT
                    || entries.get(i).getOp().getType_id() == ConstValues.GT
                    || entries.get(i).getOp().getType_id() == ConstValues.LTEQ
                    || entries.get(i).getOp().getType_id() == ConstValues.GTEQ
                    || entries.get(i).getOp().getType_id() == ConstValues.EQ) {

                if (symbolTable.existsKey(entries.get(i).getOP1().getName(), -2) == -2) {
                    if (symbolTable.getSymbol(entries.get(i).getOP1().getName(), -2).getValue() != null) {
                        //Le ponemos el valor de la variable directamente cogiéndola de la tabla de símbolos
                        entries.get(i).getOP1().setName(symbolTable.getSymbol(entries.get(i).getOP1().getName(), -2).getValue());
                        //Le ponemos tipo intg porque ya no es tipo variable
                        entries.get(i).getOP1().setType_id(ConstValues.INTG_VALUE);
                    }
                }

                if (symbolTable.existsKey(entries.get(i).getOP2().getName(), -2) == -2) {
                    if (symbolTable.getSymbol(entries.get(i).getOP2().getName(), -2).getValue() != null) {
                        //Le ponemos el valor de la variable directamente cogiéndola de la tabla de símbolos
                        entries.get(i).getOP2().setName(symbolTable.getSymbol(entries.get(i).getOP2().getName(), -2).getValue());
                        //Le ponemos tipo intg porque ya no es tipo variable
                        entries.get(i).getOP2().setType_id(ConstValues.INTG_VALUE);
                    }
                }


            }
        }
    }

    /**
     * Función de optimización que da un resultado directamente haciendo la operación.
     * Si hay dos números se podrá dar un resultado, eliminando esta línea y actualizando la siguiente línea con el valor del resultado.
     * Es decir, si en un caso se hace p=2+2 cargando el valor en un registro para luego añadirlo en p, pues con constant folding seria p=4 directamente.
     * @return Devuelve un boolean que será true si se ha aplicado optimización y false si no se ha aplicado.
     */
    private void doConstantFolding() {

        //DO CONSTANT FOLDING
        List<IntermediateTableEntry> entries = intermediateTable.getEntries();
        int countWhiles = 0;
        for (int i = 0; i < entries.size(); i++) {
            System.out.println("tuhijo puta vale " + entries.get(i).getOp().getName());
            if (entries.get(i).getOp().getName().equals("initwhile")) {
                countWhiles++;
                System.out.println("initwhile brah");
            } else if (entries.get(i).getOp().getName().equals("endwhile")) {
                countWhiles--;
            }

            if (countWhiles > 0) {
                continue;
            }

            //Si la entrada tiene operación matemática
            if ((entries.get(i).getOp().getType_id() == ConstValues.MATH_OPERATION_VALUE
                    || entries.get(i).getOp().getType_id() == ConstValues.LT
                    || entries.get(i).getOp().getType_id() == ConstValues.GT
                    || entries.get(i).getOp().getType_id() == ConstValues.LTEQ
                    || entries.get(i).getOp().getType_id() == ConstValues.GTEQ
                    || entries.get(i).getOp().getType_id() == ConstValues.EQ)
                    && (!entries.get(i+1).getOp().getName().equals("initwhile"))) {

                //System.out.println("Actual 0 is " + intermediateTable.getEntries().get(i).getOP1().getName() + " " + intermediateTable.getEntries().get(i).getOp().getName() + " " + intermediateTable.getEntries().get(i).getOP2().getName());
                //System.out.println("The type of OP1 is " + intermediateTable.getEntries().get(i).getOP1().getType_id());
                //System.out.println("The type of OP2 is " + intermediateTable.getEntries().get(i).getOP2().getType_id());
                //Y esta operación matemática está compuesta por dos numeros, entonces se suman directamente y queda optimizado.
                boolean desplega = true;
                int pos = i;
                while (desplega) {
                    //System.out.println("wiki 0");
                    if (entries.get(pos).getOP1().getType_id() == ConstValues.INTG_VALUE
                            && entries.get(pos).getOP2().getType_id() == ConstValues.INTG_VALUE
                            && entries.get(pos).getOp().getType_id() == ConstValues.MATH_OPERATION_VALUE) {
                        System.out.println("Estoy en linea " +entries.get(i).getOP1().getName()+" | "+entries.get(i).getOp().getName()+" | "+entries.get(i).getOP2().getName());
                        //System.out.println("wiki 1");
                        if (entries.get(pos).getOp().getType_id() == ConstValues.ASSIGN_EQUAL_VALUE) {
                            break;
                        }
                        //Llamamos a la función getResult de esta clase para obtener el resultado de una operación entre dos operandos
                        //System.out.println("Actual is " + intermediateTable.getEntries().get(i).getOP1().getName() + " " + intermediateTable.getEntries().get(i).getOp().getName() + " " + intermediateTable.getEntries().get(i).getOP2().getName());
                        //System.out.println("last result is " +lastResult);
                        int result = getResult(entries.get(pos).getOP1(), entries.get(pos).getOP2(), entries.get(pos).getOp());
                        //Actualizamos la siguiente línia de la tabla de código intermedio con el nuevo valor que es el resultado.
                        intermediateTable.getEntries().get(pos+1).setOP1(new RegisterToken(ConstValues.INTG_VALUE, Integer.toString(result)));
                        //Eliminamos la línia que contenia la operación porque ya no es necesaria.
                        intermediateTable.deleteEntry(pos);
                        /*System.out.println("Intermediate table 3 entries are:");
                        for (int j = 0; j < intermediateTable.size(); j++) {
                            System.out.println(intermediateTable.getEntries().get(j).getOP1().getName() + " " + intermediateTable.getEntries().get(j).getOp().getName() + " " + intermediateTable.getEntries().get(j).getOP2().getName());
                        }*/

                    } else if (entries.get(pos).getOP1().getType_id() == ConstValues.INTG_VALUE
                            && entries.get(pos).getOP2().getType_id() == ConstValues.INTG_VALUE
                            && (entries.get(i).getOp().getType_id() == ConstValues.LT
                            || entries.get(i).getOp().getType_id() == ConstValues.GT
                            || entries.get(i).getOp().getType_id() == ConstValues.LTEQ
                            || entries.get(i).getOp().getType_id() == ConstValues.GTEQ
                            || entries.get(i).getOp().getType_id() == ConstValues.EQ)) {


                        //System.out.println("ELMANOS");

                        if (entries.get(pos).getOp().getType_id() == ConstValues.ASSIGN_EQUAL_VALUE
                                || entries.get(pos).getOp().getType_id() == ConstValues.IF) {
                            break;
                        }

                        boolean result = getComparation(entries.get(pos).getOp(),
                                Integer.valueOf(entries.get(pos).getOP1().getName()),
                                Integer.valueOf(entries.get(pos).getOP2().getName()));
                        //Actualizamos la siguiente línia de la tabla de código intermedio con el nuevo valor que es el resultado.
                        intermediateTable.getEntries().get(pos+1).setOP1(new RegisterToken(ConstValues.INTG_VALUE, Boolean.toString(result)));
                        //System.out.println("Acabamos de poner " + Boolean.toString(result) + " al resultado " + result);
                        //Eliminamos la línia que contenia la operación porque ya no es necesaria.
                        intermediateTable.deleteEntry(pos);

                    } else {
                        desplega = false;
                    }
                }
            }
        }
    }
}
