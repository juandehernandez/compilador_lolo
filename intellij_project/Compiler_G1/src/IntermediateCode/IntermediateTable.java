package IntermediateCode;

import Lexicon.ConstValues;
import Lexicon.Token;
import Symbols.SymbolTable;

import java.util.ArrayList;

/**
 * Tabla para generar código intermedio
 */
public class IntermediateTable {
    private ArrayList<IntermediateTableEntry> entries;
    private static int counter;
    private SymbolTable symbolTable;

    public IntermediateTable(SymbolTable symbolTable) {
        entries = new ArrayList<IntermediateTableEntry>();
        counter = 1;
        this.symbolTable = symbolTable;
    }

    public int size(){
        return entries.size();
    }

    public ArrayList<IntermediateTableEntry> getEntries() {
        return entries;
    }

    public Token addEntry(Token OP1, Token OP2, Token op) {
        //Check what type is going to be the RegisterToken (Example intg * real -> RegisterToken will be type real)
        RegisterToken R = newRegisterToken(OP1, OP2, op);

        // Generate the new entry of the table
        IntermediateTableEntry entry = new IntermediateTableEntry(OP1, OP2, op, R);

        // Add the new entry to the table
        entries.add(entry);

        // return the token of the register where the value will be stored
        return R;
    }

    public void deleteEntry(int position) {
        entries.remove(position);
    }

    private RegisterToken newRegisterToken(Token OP1, Token OP2, Token op) {
        // Obtenemos el tipo de los dos OPERANDOS (entero, string, char, bool...)
        int typeOP1 = getTokenType(OP1);
        int typeOP2 = getTokenType(OP2);

        if (typeOP1 == -1 || typeOP2 == -1){
            /*
                Alguno de los dos operandos (o los dos) es una variable y no se encuentra en la tabla de simbolos

                NUNCA debería entrar aquí porque eso lo controla el ANALIZADOR SEMÁNTICO
             */
            return null;
        }

        int resultType = mixTypes(typeOP1, typeOP2);

        if (resultType == -1){
            // NUNCA debería entrar aquí, controlado por el semántico
            return null;
        }else{
            // Creamos el nuevo token
            RegisterToken R = new RegisterToken(resultType, "R" + counter);
            counter++;

            return R;
        }

    }

    private int mixTypes(int typeOP1, int typeOP2) {
        // TODO: Añadir en un futuro las combinaciones entre entero y real
        if (typeOP1 == ConstValues.INTG_VALUE && typeOP2 == ConstValues.INTG_VALUE){
            return ConstValues.INTG_VALUE; // Operación entre 2 enteros
        }else{
            return -1; // Error
        }
    }

    private int getTokenType(Token OP) {
        if (isValue(OP)){
            return OP.getType_id();
        }else{
            String name = OP.getName();
            int scope = OP.getScope();

            if (existsSymbol(name, scope)){
                return symbolTable.getSymbol(name, scope).getType();
            }else{
                return -1;
            }
        }
    }

    private boolean existsSymbol(String name, int scope) {
        int scopeReturn = symbolTable.existsKey(name, scope);

        if (scopeReturn == SymbolTable.NOT_EXISTS)
            return false;

        if (scopeReturn == SymbolTable.NOT_CREATED)
            return false;

        return true;
    }

    private boolean isValue(Token OP) {
        int type = OP.getType_id();
        if (type == ConstValues.NAME){
            return false;
        }else {
            return true;
        }
    }
}
