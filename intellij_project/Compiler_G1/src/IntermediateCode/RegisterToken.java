package IntermediateCode;

import Lexicon.Token;

public class RegisterToken extends Token {
    public RegisterToken(int type_id, String name) {
        super(type_id, name);
    }
}
