package IntermediateCode;

import Lexicon.Token;

/**
 *  Entry of the intermediate table.
 */
public class IntermediateTableEntry {
    private Token OP1;
    private Token OP2;
    private Token op;
    private Token R;


    /**
     *  Constructor for a table entry.
     * @param OP1 First Operand
     * @param OP2 Second Operand
     * @param op Operator of the two operands
     */
    public IntermediateTableEntry (Token OP1, Token OP2, Token op, Token R) {
        this.OP1 = OP1;
        this.OP2 = OP2;
        this.op = op;
        this.R = R;
    }

    //Getters
    public Token getR() {
        return R;
    }

    public Token getOp() {
        return op;
    }

    public Token getOP1() {
        return OP1;
    }

    public Token getOP2() {
        return OP2;
    }

    //Setters
    public void setR(Token r) {
        R = r;
    }

    public void setOp(Token op) {
        this.op = op;
    }

    public void setOP1(Token OP1) {
        this.OP1 = OP1;
    }

    public void setOP2(Token OP2) {
        this.OP2 = OP2;
    }
}
