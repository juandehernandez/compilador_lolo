import CodeOptimization.CodeOptimizer;
import Error.Error;
import Error.ErrorHandler;
import Mips.MipsGenerator;
import Symbols.SymbolTable;
import Syntactic.Parser;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.LinkedTransferQueue;

/**
 * Lolo language compiler
 */
public class LoLoC {

    private String sourceFile;
    private String outputFile;

    public LoLoC(String sourceFile, String outputFile){
        this.sourceFile = sourceFile;
        this.outputFile = outputFile;
    }

    /*public void compile(){

        SymbolTable symbolTable = new SymbolTable();
        // Initialize queues

        try {
            //parser = new Parser(scannerOutParserInQueue);
            ErrorHandler errorHandler = new ErrorHandler();
            Parser parser = new Parser(symbolTable, sourceFile, errorHandler);
            boolean correct = parser.run();
            if (correct){
                // Everything in code is correct
                CodeOptimizer codeOptimizer = new CodeOptimizer(parser.getParseTree(), symbolTable);
                MipsGenerator assemblyGenerator = codeOptimizer.getMipsGenerator();
                codeOptimizer.optimize();
                assemblyGenerator.generateMipsCode();
                assemblyGenerator.dumpCodeIntoFile(outputFile);
            } else {
                errorHandler.dumpAllErrors();
            }

        } catch (FileNotFoundException e) {
            System.out.println("Impossible to compile. File does not exist!");
            e.printStackTrace();
        } catch (Error.ErrorTypeException | IOException e) {
            e.printStackTrace();
        }


    }
*/
}
