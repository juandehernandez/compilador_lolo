package Symbols;

import Lexicon.Token;

import java.util.ArrayList;
import java.util.HashMap;

import static Lexicon.ConstValues.GLOBAL_SCOPE;
import static Lexicon.ConstValues.MAIN_SCOPE;


public class SymbolTable{

    // Inner class
    public class Symbol{
        // Attributes
        private String name;
        private int type; // (variable, function)
        private String value;

        // Constructor
        public Symbol(){}
        public Symbol(String name, int type){
            this.name = name;
            this.type = type;
        }
        public Symbol(Token token){
            this.type = token.getType_id();
            this.name = token.getName();
        }

        // Getters & Setters
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }
        public int getType() {
            return type;
        }
        public void setType(int type) {
            this.type = type;
        }
        public String getValue() {
            return value;
        }
        public void setValue(String value) {
            this.value = value;
        }
    }

    // Constant values
    public static final int NOT_EXISTS = -3;
    public static final int NOT_CREATED = -4;
    public static final int MIPS_AUX = -5;


    // Attributes
    private HashMap<String, Symbol> globalScope;
    private HashMap<String, Symbol> mainScope;
    private ArrayList<HashMap<String, Symbol>> functionScopes;
    private HashMap<String, Symbol> mipsAux;


    // Constructor
    public SymbolTable(){
        globalScope = new HashMap<String, Symbol>();
        mainScope = new HashMap<String, Symbol>();
        functionScopes = new ArrayList<HashMap<String, Symbol>>();
        mipsAux = new HashMap<String, Symbol>();
    }


    // Getters & Setters
    public HashMap getGlobalScope() {
        return globalScope;
    }
    public void setGlobalScope(HashMap globalScope) {
        this.globalScope = globalScope;
    }
    public HashMap getMainScope() {
        return mainScope;
    }
    public void setMainScope(HashMap mainScope) {
        this.mainScope = mainScope;
    }
    public ArrayList<HashMap<String, Symbol>> getFunctionScopes() {
        return functionScopes;
    }
    public void setFunctionScopes(ArrayList<HashMap<String, Symbol>> functionScopes) {
        this.functionScopes = functionScopes;
    }


    // Functions
    public Symbol createSymbol(Token token){
        return new Symbol(token);
    }

    public boolean existsTable(int scope){
        if (scope == GLOBAL_SCOPE || scope == MAIN_SCOPE || scope == MIPS_AUX){
            return true;
        }
        return scope < functionScopes.size();
    }

    public int createNewScope(){
        functionScopes.add(new HashMap<String, Symbol>());
        return functionScopes.size() - 1; //We return the index of the new table
    }

    /**
     * Función que retorna el scope en el que se encuentra la variable con nombre 'key'
     * En caso de no existir en ninguno de los scopes encadenados al scope actual, retornará NOT_EXISTS
     * En caso de no existir el scope actual, retornará NOT_CREATED
     * @param key : Nombre de la variable a buscar
     * @param scope : Scope en el que se encuentra la variable
     * @return Scope / NOT_EXISTS / NOT_CREATED
     */
    public int existsKey(String key, int scope){
        int b = NOT_EXISTS;

        switch (scope){
            case GLOBAL_SCOPE:
                // Tan solo mirar el scope global
                if (globalScope.get(key) != null){
                    b = GLOBAL_SCOPE;
                }else {
                    b = NOT_EXISTS;
                }
                break;

            case MAIN_SCOPE:
                /* Mirar:
                     - Scope main
                     - Scope global
                 */
                if (mainScope.get(key) != null){
                    b = MAIN_SCOPE;
                }else if (globalScope.get(key) != null){
                    b = GLOBAL_SCOPE;
                }else{
                    b = NOT_EXISTS;
                }
                break;
            case MIPS_AUX:
                if(mipsAux.get(key) != null){
                    b =  MIPS_AUX;
                }else{
                    b = NOT_EXISTS;
                }
                break;

            default:
                /* Mirar:
                     - Scope actual
                     - Scope main
                     - Scope global
                 */
                if (!existsTable(scope)){
                    b = NOT_CREATED;
                }else{
                    if (functionScopes.get(scope).get(key) != null){
                        b = scope;
                    }else if (mainScope.get(key) != null){
                        b = MAIN_SCOPE;
                        if (globalScope.get(key) != null){
                            b = GLOBAL_SCOPE;
                        }else{
                            b = NOT_EXISTS;
                        }
                    }
                }
                break;
        }
        return b;
    }


    /**
     * Función que inserta una nueva variable en la tabla de símbolos que le corresponde
     * En el caso de que no exista anteriormente se devuelve TRUE.
     * En el caso de que exista anteriormente, se devuelve FALSE.
     * @param token : token referente a la variable
     * @param scope : scope en el que intentamos insertar la nueva variable
     * @return TRUE / FALSE
     */
    public boolean insertKey(Token token, int scope){
        String key = token.getName();
        if (!existsTable(scope)){
            scope = createNewScope();
        }
        int scopeFound = existsKey(key, scope);

        if (scopeFound == NOT_EXISTS){
            // Insertamos la nueva variable en el scope que se ha pedido
            Symbol symbol = new Symbol(token);
            switch (scope){
                case GLOBAL_SCOPE:
                    globalScope.put(key, symbol);
                    break;
                case MAIN_SCOPE:
                    mainScope.put(key, symbol);
                    break;
                case MIPS_AUX:
                    mipsAux.put(key, symbol);
                    break;
                default:
                    functionScopes.get(scope).put(key, symbol);
                    break;
            }
            return true;
        }

        return false; // Not inserted again because it has been inserted before and: ERROR -> It's not possible to declare the same
    }


    /**
     * Función que retorna el symbolo albergado según la key y el scope especificado.
     * @param key : identificador del simbolo
     * @param scope : scope donde se encuentra el simbolo
     * @return Symbol
     */
    public Symbol getSymbol(String key, int scope){
        if (scope == GLOBAL_SCOPE){
            return globalScope.get(key);
        }else if (scope == MAIN_SCOPE){
            return mainScope.get(key);
        }else{
            return functionScopes.get(scope).get(key);
        }
    }

}
